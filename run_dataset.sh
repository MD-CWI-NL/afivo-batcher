#! /bin/bash

if [ $# -ne 3 ]
then
  echo "Not enough arguments. Correct usage: "$0" EXECUTABLE DATASET NUM_THREADS"
  exit
fi

executable=$1
dataset=$2
num_threads=$3

export OMP_NUM_THREADS=$num_threads
echo "Num threads: " $OMP_NUM_THREADS


dataset_dir=$(dirname "${dataset}")
dataset_name=$(basename "${dataset}")

executable_dir="$( cd "$( dirname "$executable" )" && pwd )"
executable_name=$(basename "${executable}")
executable_full_path=$executable_dir"/"$executable_name

cd $dataset_dir

cat $dataset_name | while read line
do
  echo $line
  $executable_full_path $line
done
