from distutils.core import setup
import sys

if sys.version_info < (3, 0):
    sys.exit('Python version 3.0 or higher is required.')

setup(name='afivo-batcher',
      version='1.0',
      description="Collection of classes to make it simpler to run 100s of simulations using afivo derived programs.",
      author="Andy Martinez",
      author_email="Andy.Martinez@cwi.nl",
      url="https://gitlab.com/MD-CWI-NL/afivo-batcher",
      packages=['afivobatcher', 'afivobatcher.configreader', 'afivobatcher.runner'],
      )