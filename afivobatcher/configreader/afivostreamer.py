from afivobatcher.configreader.general import *


def get_field_amplitude_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_amplitude", cfg_string)


def get_field_background_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_background", cfg_string)


def get_field_bc_type_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("field_bc_type", cfg_string)


def get_field_decay_time_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_decay_time", cfg_string)


def get_field_lin_deriv_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_lin_deriv", cfg_string)


def get_field_mod_t0_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_mod_t0", cfg_string)


def get_field_point_charge_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_point_charge", cfg_string)


def get_field_point_r0_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("field_point_r0", cfg_string)


def get_field_sin_amplitude_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_sin_amplitude", cfg_string)


def get_field_sin_freq_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_sin_freq", cfg_string)


def get_field_stability_search_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("field_stability_search", cfg_string)


def get_field_stability_threshold_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_stability_threshold", cfg_string)


def get_field_stability_zmax_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_stability_zmax", cfg_string)


def get_field_stability_zmin_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("field_stability_zmin", cfg_string)


def get_derefine_cphi_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("derefine_cphi", cfg_string)


def get_derefine_dx_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("derefine_dx", cfg_string)


def get_refine_adx_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_adx", cfg_string)


def get_refine_adx_fac_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_adx_fac", cfg_string)


def get_refine_buffer_width_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_buffer_width", cfg_string)


def get_refine_cphi_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_cphi", cfg_string)


def get_refine_init_fac_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_init_fac", cfg_string)


def get_refine_init_time_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_init_fac", cfg_string)


def get_refine_max_dx_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_max_dx", cfg_string)


def get_refine_min_dens_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_min_dens", cfg_string)


def get_refine_min_dx_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_min_dx", cfg_string)


def get_refine_per_steps_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_per_steps", cfg_string)


def get_refine_regions_dr_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("refine_regions_dr", cfg_string)


def get_refine_regions_rmax_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("refine_regions_rmax", cfg_string)


def get_refine_regions_rmin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("refine_regions_rmin", cfg_string)


def get_refine_regions_tstop_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_regions_tstop", cfg_string)


def get_refine_limits_dr_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_limits_dr", cfg_string)


def get_refine_limits_rmin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("refine_limits_rmin", cfg_string)


def get_refine_limits_rmax_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("refine_limits_rmax", cfg_string)


def get_background_density_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("background_density", cfg_string)


def get_stochastic_density_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("stochastic_density", cfg_string)


def get_prolong_density_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("prolong_density", cfg_string)


def get_silo_write_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("silo_write", cfg_string)


def get_dt_max_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt_max", cfg_string)


def get_dt_min_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt_min", cfg_string)


def get_end_time_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("end_time", cfg_string)


def get_dt_chemistry_nmin_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt_chemistry_nmin", cfg_string)


def get_dt_safety_factor_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt_safety_factor", cfg_string)


def get_multigrid_num_vcycles_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("multigrid_num_vcycles", cfg_string)


def get_box_size_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("box_size", cfg_string)


def get_use_end_time_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("use_end_time", cfg_string)


def get_use_end_streamer_length_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("use_end_streamer_length", cfg_string)


def get_end_streamer_length_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("end_streamer_length", cfg_string)


def get_time_integrator_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("time_integrator", cfg_string)


def get_restart_from_file_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("restart_from_file", cfg_string)


def get_memory_limit_GB_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("memory_limit_GB", cfg_string)


def get_rng_seed_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("rng_seed", cfg_string)


def get_periodic_from_cfg_string(cfg_string):

    return get_bool_list_from_cfg_string("periodic", cfg_string)


def get_cylindrical_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("cylindrical", cfg_string)


def get_use_dielectric_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("use_dielectric", cfg_string)


def get_domain_len_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("domain_len", cfg_string)


def get_coarse_grid_size_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("coarse_grid_size", cfg_string)


def get_domain_origin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("domain_origin", cfg_string)


def get_lineout_npoints_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("npoints", cfg_string, "lineout")


def get_lineout_rmax_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("rmax", cfg_string, "lineout")


def get_lineout_rmin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("rmin", cfg_string, "lineout")


def get_lineout_write_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("write", cfg_string, "lineout")


def get_photoi_enabled_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("enabled", cfg_string, "photoi")


def get_photoi_eta_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("eta", cfg_string, "photoi")


def get_photoi_method_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("method", cfg_string, "photoi")


def get_photoi_per_steps_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("per_steps", cfg_string, "photoi")


def get_photoi_quenching_pressure_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("quenching_pressure", cfg_string, "photoi")


def get_photoi_source_type_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("source_type", cfg_string, "photoi")


def get_photoi_excited_species_from_cfg_string(cfg_string):

    return get_string_list_from_cfg_string("excited_species", cfg_string, "photoi")


def get_photoi_photoemission_time_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("photoemission_time", cfg_string, "photoi")


def get_photoi_helm_author_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("author", cfg_string, "photoi_helmh")


def get_photoi_helm_coeffs_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("coeffs", cfg_string, "photoi_helmh")


def get_photoi_helm_lambdas_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("lambdas", cfg_string, "photoi_helmh")


def get_photoi_helm_max_rel_residual_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("max_rel_residual", cfg_string, "photoi_helmh")


def get_photoi_mc_absorp_fac_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("absorp_fac", cfg_string, "photoi_mc")


def get_photoi_mc_const_dx_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("const_dx", cfg_string, "photoi_mc")


def get_photoi_mc_min_dx_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("min_dx", cfg_string, "photoi_mc")


def get_photoi_mc_num_photons_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("num_photons", cfg_string, "photoi_mc")


def get_photoi_mc_physical_photons_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("physical_photons", cfg_string, "photoi_mc")


def get_plane_npixels_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("npixels", cfg_string, "plane")


def get_plane_rmax_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("rmax", cfg_string, "plane")


def get_plane_rmin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("rmin", cfg_string, "plane")


def get_plane_write_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("write", cfg_string, "plane")


def get_plane_varname_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("varname", cfg_string, "plane")


def get_fixes_diffusion_field_limit_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("diffusion_field_limit", cfg_string, "fixes")


def get_fixes_drt_limit_flux_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("drt_limit_flux", cfg_string, "fixes")


def get_fixes_max_velocity_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("max_velocity", cfg_string, "fixes")
 
 
def get_gas_components_from_cfg_string(cfg_string):

    return get_string_list_from_cfg_string("components", cfg_string, "gas")


def get_gas_fractions_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("fractions", cfg_string, "gas")


def get_gas_pressure_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("pressure", cfg_string, "gas")


def get_gas_temperature_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("temperature", cfg_string, "gas")


def get_output_dt_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt", cfg_string, "output")


def get_output_name_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("name", cfg_string, "output")


def get_output_src_decay_time_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("src_decay_time", cfg_string, "output")


def get_output_src_term_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("src_term", cfg_string, "output")


def get_output_status_delay_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("status_delay", cfg_string, "output")


def get_output_only_from_cfg_string(cfg_string):

    return get_string_list_from_cfg_string("only", cfg_string, "output")


def get_datfile_write_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("write", cfg_string, "datfile")


def get_datfile_per_outputs_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("per_outputs", cfg_string, "datfile")


def get_table_data_max_townsend_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("max_townsend", cfg_string, "table_data")


def get_table_data_min_townsend_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("min_townsend", cfg_string, "table_data")


def get_table_data_size_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("size", cfg_string, "table_data")


def get_input_data_file_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("file", cfg_string, "input_data")


def get_input_data_old_style_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("old_style", cfg_string, "input_data")


def get_field_maxima_write_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("write", cfg_string, "field_maxima")


def get_field_maxima_distance_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("distance", cfg_string, "field_maxima")


def get_field_maxima_threshold_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("threshold", cfg_string, "field_maxima")


def get_silo_per_outputs_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("per_outputs", cfg_string, "silo")


def get_use_circuit_andy_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("use_circuit_andy", cfg_string)

def get_use_circuit_R_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("use_circuit_R", cfg_string)

def get_use_top_electrode_sphere_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("use_top_electrode_sphere", cfg_string)

def get_use_bottom_electrode_sphere_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("use_bottom_electrode_sphere", cfg_string)

def get_top_electrode_sphere_radius_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("top_electrode_sphere_radius", cfg_string)

def get_bottom_electrode_sphere_radius_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("bottom_electrode_sphere_radius", cfg_string)

def get_capacitor_capacitance_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("capacitor_capacitance", cfg_string)

def get_resistor_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("resistor", cfg_string)

def get_gnd_resistor_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("gnd_resistor", cfg_string)

def get_capacitor_initial_voltage_fraction_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("capacitor_initial_voltage_fraction", cfg_string)

def get_top_electrode_initial_voltage_fraction_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("top_electrode_initial_voltage_fraction", cfg_string)

def get_n_streamer_circuit_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("n_streamers_circuit", cfg_string)

def get_species_boundary_condition_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("species_boundary_condition", cfg_string)

def get_t_field_off_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("t_field_off", cfg_string)

def get_seeds_from_cfg_string(cfg_string):
    # Get all seed variables
    seeds_charge_type = get_float_list_from_cfg_string("seed_charge_type", cfg_string)
    seeds_density = get_float_list_from_cfg_string("seed_density", cfg_string)
    seeds_falloff = get_string_list_from_cfg_string("seed_falloff", cfg_string)
    seeds_rel_r0 = get_float_list_from_cfg_string("seed_rel_r0", cfg_string)
    seeds_rel_r1 = get_float_list_from_cfg_string("seed_rel_r1", cfg_string)
    seeds_width = get_float_list_from_cfg_string("seed_width", cfg_string)

    # Create a seed dict for every seed
    num_seeds = len(seeds_charge_type)
    n_dim = int(len(seeds_rel_r0) / num_seeds)
    seeds = []

    for i in range(num_seeds):
        seed = {}
        seed["seed_charge_type"] = seeds_charge_type[i]
        seed["seed_density"] = seeds_density[i]
        seed["seed_falloff"] = seeds_falloff[i]
        seed["seed_width"] = seeds_width[i]
        seed["seed_rel_r0"] = list(seeds_rel_r0[int(i * n_dim):n_dim])
        seed["seed_rel_r1"] = list(seeds_rel_r1[int(i * n_dim):n_dim])

        seeds.append(seed)

    return seeds


def get_seed_charge_type_from_seed(seed):

    return seed["seed_charge_type"]


def get_seed_density_from_seed(seed):

    return seed["seed_density"]


def get_seed_falloff_from_seed(seed):

    return seed["seed_falloff"]


def get_seed_width_from_seed(seed):

    return seed["seed_width"]


def get_seed_rel_r0_from_seed(seed):

    return seed["seed_rel_r0"]


def get_seed_rel_r1_from_seed(seed):

    return seed["seed_rel_r1"]

