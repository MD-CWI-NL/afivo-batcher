from afivobatcher.utils import afivo_cfg_bool_to_python


#################### List fetch functions ###############

def get_float_list_from_cfg_string(cfg_var, cfg_string, cfg_var_category=None):
    cfg_val_split = _get_list_from_cfg_string(cfg_var, cfg_string, cfg_var_category)

    return [float(x) for x in cfg_val_split]


def get_bool_list_from_cfg_string(cfg_var, cfg_string, cfg_var_category=None):
    cfg_val_split = _get_list_from_cfg_string(cfg_var, cfg_string, cfg_var_category)

    return [afivo_cfg_bool_to_python(x) for x in cfg_val_split]


def get_string_list_from_cfg_string(cfg_var, cfg_string, cfg_var_category=None):
    cfg_val_split = _get_list_from_cfg_string(cfg_var, cfg_string, cfg_var_category)

    return [str(x).replace("'", "") for x in cfg_val_split]

#########################################################

#################### Single value fetch functions #######

def get_bool_from_cfg_string( cfg_var, cfg_string, cfg_var_category=None):
    cfg_var_value = _get_var_value_from_cfg_string(cfg_var, cfg_string, cfg_var_category)

    return afivo_cfg_bool_to_python(cfg_var_value)


def get_float_from_cfg_string(cfg_var, cfg_string, cfg_var_category=None):
    cfg_var_value = _get_var_value_from_cfg_string(cfg_var, cfg_string, cfg_var_category)

    return float(cfg_var_value)


def get_string_from_cfg_string(cfg_var, cfg_string, cfg_var_category=None):
    cfg_var_value = _get_var_value_from_cfg_string(cfg_var, cfg_string, cfg_var_category).replace("'", "")

    return str(cfg_var_value)

#########################################################


def _sanitize_cfg_string(cfg_string):
    split_cfg_string = cfg_string.split("\n\n")

    # Remove initial '## USER##' or '### DEFAULT ##' line and a final empty line
    split_cfg_string = split_cfg_string[1:-2]

    # Split all variables from each other
    seperated_variables = []
    for cfg_element in split_cfg_string:
        split_cfg_element = cfg_element.split("\n")
        for element in split_cfg_element:
            if element:
                seperated_variables.append(element.rstrip())

    return seperated_variables


def _get_var_value_from_cfg_string(cfg_var, cfg_string, cfg_var_category):

    cfg_var_to_fetch = cfg_var

    split_cfg_string = _sanitize_cfg_string(cfg_string)

    if cfg_var_category:
        cfg_var_to_fetch = cfg_var_category + "%" + cfg_var

    for cfg_element in split_cfg_string:

        if cfg_var_to_fetch == cfg_element.split()[0]:
            cfg_element_split = cfg_element.split(" = ")
            cfg_element_var = cfg_element_split[0]
            cfg_element_val = cfg_element_split[1]

            return cfg_element_val


def _get_list_from_cfg_string(cfg_var, cfg_string, cfg_var_category):

    cfg_var_value = _get_var_value_from_cfg_string(cfg_var, cfg_string, cfg_var_category)

    cfg_val_split = cfg_var_value.split()

    return cfg_val_split