from afivobatcher.configreader.general import  *


def get_domain_rmin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("rmin", cfg_string, "domain")


def get_domain_rmax_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("rmax", cfg_string, "domain")


def get_domain_size_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("size", cfg_string, "domain")


def get_vtu_length_scale_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("length_scale", cfg_string, "vtu")


def get_vtu_var_name_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("var_name", cfg_string, "vtu")


def get_vtu_file_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("file", cfg_string, "vtu")


def get_vtu_epsilon_file_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("epsilon_file", cfg_string, "vtu")


def get_vtu_refine_voltage_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("refine_voltage", cfg_string, "vtu")


def get_vtu_interpolation_script_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("interpolation_script", cfg_string, "vtu")


def get_vtu_epsilon_gas_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("epsilon_gas", cfg_string, "vtu")


def get_init_electron_distribution_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("electron_distribution", cfg_string, "init")


def get_init_electron_density_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("electron_density", cfg_string, "init")


def get_init_electron_sphere_center_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("electron_sphere_center", cfg_string, "init")


def get_init_electron_sphere_radius_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("electron_sphere_radius", cfg_string, "init")


def get_init_electron_cylinder_rmin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("electron_cylinder_rmin", cfg_string, "init")


def get_init_electron_cylinder_rmax_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("electron_cylinder_rmax", cfg_string, "init")


def get_init_electron_z_coord_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("electron_z_coord", cfg_string, "init")


def get_init_ion_distribution_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("ion_distribution", cfg_string, "init")


def get_init_ion_density_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_density", cfg_string, "init")


def get_init_ion_sphere_center_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("ion_sphere_center", cfg_string, "init")


def get_init_ion_sphere_radius_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_sphere_radius", cfg_string, "init")


def get_init_ion_cylinder_rmin_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("ion_cylinder_rmin", cfg_string, "init")


def get_init_ion_cylinder_rmax_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_cylinder_rmax", cfg_string, "init")


def get_init_ion_z_coord_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_z_coord", cfg_string, "init")


def get_init_ion_distribution2_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("ion_distribution2", cfg_string, "init")


def get_init_ion_density2_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_density2", cfg_string, "init")


def get_init_ion_sphere_center2_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("ion_sphere_center2", cfg_string, "init")


def get_init_ion_sphere_radius2_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_sphere_radius2", cfg_string, "init")


def get_init_ion_cylinder_rmin2_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("ion_cylinder_rmin2", cfg_string, "init")


def get_init_ion_cylinder_rmax2_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_cylinder_rmax2", cfg_string, "init")


def get_init_ion_z_coord2_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("ion_z_coord2", cfg_string, "init")


def get_simulation_electron_field_threshold_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("electron_field_threshold", cfg_string, "simulation")


def get_simulation_dt_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt", cfg_string, "simulation")


def get_simulation_dt_ions_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt_ions", cfg_string, "simulation")


def get_simulation_end_time_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("end_time", cfg_string, "simulation")


def get_simulation_max_num_electrons_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("max_num_electrons", cfg_string, "simulation")


def get_simulation_n_runs_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("n_runs", cfg_string, "simulation")


def get_simulation_box_size_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("box_size", cfg_string, "simulation")


def get_simulation_cs_file_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("cs_file", cfg_string, "simulation")


def get_output_name_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("name", cfg_string, "output")


def get_output_dt_particles_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("dt_particles", cfg_string, "output")


def get_output_log_step_interval_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("log_step_interval", cfg_string, "output")


def get_output_n_particles_step_interval_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("n_particles_step_interval", cfg_string, "output")


def get_output_n_particles_over_time_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("n_particles_over_time", cfg_string, "output")


def get_output_particles_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("particles", cfg_string, "output")


def get_output_log_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("log", cfg_string,  "output")


def get_output_last_electron_from_cfg_string(cfg_string):

    return get_bool_from_cfg_string("last_electron", cfg_string, "output")


def get_gas_pressure_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("pressure", cfg_string, "gas")


def get_gas_temperature_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("temperature", cfg_string, "gas")


def get_grid_tree_dat_file_from_cfg_string(cfg_string):

    return get_string_from_cfg_string("tree_dat_file", cfg_string, "grid")


def get_ion_mass_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("mass", cfg_string, "ion")


def get_ion_reduced_mobility_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("reduced_mobility", cfg_string, "ion")


def get_ion_max_eV_from_cfg_string(cfg_string):

    return get_float_from_cfg_string("max_eV", cfg_string, "ion")


def get_voltage_amplitudes_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("amplitudes", cfg_string, "voltage")


def get_voltage_times_from_cfg_string(cfg_string):

    return get_float_list_from_cfg_string("times", cfg_string, "voltage")