import os


def python_bool_to_afivo_cfg_str(bool_val):
    if bool_val is True:
        return "T"
    else:
        return "F"


def afivo_cfg_bool_to_python(afivo_bool):

    if afivo_bool == "T":
        return True
    else:
        return False


def python_list_to_afivo_cfg_str(list_val):
    list_cfg_str = ""

    if len(list_val) > 0 and type(list_val[0]) == bool:
        for val in list_val:
            list_cfg_str += python_bool_to_afivo_cfg_str(val) + " "
    else:
        for val in list_val:
            list_cfg_str += str(val) + " "

    return list_cfg_str


def get_absolute_paths(val_string):

    absolute_path_val = None

    if type(val_string) == list:
        absolute_path_val_list = []

        for val in val_string:
            if val[0] == "~":
                val = os.path.expanduser(val)
            val= os.path.abspath(val)

            absolute_path_val_list.append(val)

        absolute_path_val = absolute_path_val_list
    else:
        absolute_path_val = val_string
        if absolute_path_val[0] == "~":
            absolute_path_val = os.path.expanduser(absolute_path_val)
        absolute_path_val = os.path.abspath(absolute_path_val)

    return absolute_path_val
