import os
import subprocess
import math


def custom_walltime_example(cfg_str):
    hours = 24
    minutes = 0
    seconds = 0

    return hours, minutes, seconds


def custom_memory_example(cfg_str):
    memory = 64

    return memory


def custom_cores_example(cfg_str):
    cores = 16

    return cores


def custom_nodes_example(cfg_str):
    nodes = 1

    return nodes


def custom_partition_example(cfg_str):
    partition = "normal"

    return partition


class RemoteScheduler:

    def __init__(self, remote_username, remote_host, remote_path_to_executable, local_path_to_dataset, remote_path_to_script_destination, copy_scripts_with_custom_dir=False, remote_job_scheduler="slurm"):
        print("THIS NEEDS TO BE EXECUTED FROM A TERMINAL")
        self._remote_host = remote_host
        self._remote_username = remote_username

        self._dataset_filename = self._set_dataset_filename(local_path_to_dataset)
        self._job_script_local_dataset_filename = ""
        self._job_script_remote_dataset_filename = ""
        self._remote_executable_path = remote_path_to_executable

        # Sets where the job scripts and config files will be copied to on the remote host
        # before submitting the jobs.
        self._remote_script_dir = remote_path_to_script_destination

        # If custom directories were made for each config you can set "copy_scripts_with_custom_dir" to True
        # so that these custom directories will be copied to the remote_dir as well.
        self._copy_custom_script_dir = copy_scripts_with_custom_dir

        # PBS or SLURM
        self._remote_job_scheduler = remote_job_scheduler.lower()
        self._command_prefix_job_scheduler = {"slurm": {"submit": "sbatch",
                                                        "prefix": "#SBATCH",
                                                        "jobname": " --job-name=",
                                                        "walltime": " -t ",
                                                        "nodes": " -N ",
                                                        "partition": " -p ",
                                                        "cores": " -c ",
                                                        "memory": " --mem=",
                                                        "output": " --output=",
                                                        "error": " --error="},
                                              "pbs": {}}

        self._custom_job_commands_before_exec = []
        self._custom_job_commands_during_exec = []
        self._custom_job_commands_after_exec = []
        self._custom_walltime_func = None
        self._custom_memory_func = None
        self._custom_cores_func = None
        self._custom_nodes_func = None
        self._custom_partition_func = None

        self._walltime = "24:00:00"
        self._memory = "60G"
        self._cores = "16"
        self._nodes = "1"
        self._partition = "normal"

        self._n_simulations_per_job = 1

        self._move_output_to_job_dir = False
        self._remote_output_dir = ""

        self._timeout = -1.0
        self._timeout_before_walltime_is_set = False

    def _hours_from_walltime_str(self, walltime_str):

        split = walltime_str.split(":")

        hours = float(split[0])
        minutes = float(split[1])
        seconds = float(split[2])

        return hours + (1.0 / 60.0) * minutes + (1.0 / 3600.0) * seconds

    def _set_dataset_filename(self, path_to_dataset):

        dataset_filename = path_to_dataset

        if path_to_dataset[0] == "~":
            dataset_filename = os.path.expanduser(path_to_dataset)
        dataset_filename = os.path.abspath(dataset_filename)

        if ".set" not in dataset_filename:
            dataset_filename = dataset_filename + ".set"

        return dataset_filename

    def set_n_simulations_per_job(self, n):
        """
        Number of simulations to run per job.
        This will also change the number of openMP threads
        to be split between simulations.
        """

        self._n_simulations_per_job = n

    def set_walltime_from_cfg_str_func(self, walltime_func):
        self._custom_walltime_func = walltime_func

    def set_walltime(self, hours=0, minutes=0, seconds=0):
        self._walltime = str(hours) + ":" + str(minutes) + ":" + str(seconds)

    def set_timeout_before_walltime(self, hours=0, minutes=0, seconds=0):
        self._timeout_before_walltime_is_set = True

        total_hours = hours + (1.0 / 60.0) * minutes + (1.0 / 3600.0) * seconds

        self._timeout = total_hours

    def set_memory_from_cfg_str_func(self, memory_func):
        self._custom_memory_func = memory_func

    def set_memory(self, mem_in_gb):
        self._memory = str(mem_in_gb) + "G"

    def set_cores_from_cfg_str_func(self, cores_func):
        self._custom_cores_func = cores_func

    def set_cores(self, n_cores):
        self._cores = "%.0i" % n_cores

    def set_nodes_from_cfg_str_func(self, nodes_func):
        self._custom_nodes_func = nodes_func

    def set_nodes(self, n_nodes):
        self._nodes = "%.0i" % n_nodes

    def set_partition_from_cfg_str_func(self, partition_func):
        self._custom_partition_func = partition_func

    def set_partition(self, partition):
        self._partition = partition

    def add_custom_command_to_job_before_exec(self, custom_command):
        self._custom_job_commands_before_exec.append(custom_command)

    def add_custom_command_to_job_during_exec(self, custom_command):
        self._custom_job_commands_during_exec.append(custom_command)

    def add_custom_command_to_job_after_exec(self, custom_command):
        self._custom_job_commands_after_exec.append(custom_command)

    def move_output_on_remote_to_job_script_dir(self, remote_output_dir):
        """
        Calling this function will add an extra line in the job scripts after
        execution of the program. This extra line takes care of moving the output
        from the remote output directory (if you know it, e.g. /scratch) to the directory of the job script on the remote host.
        """
        self._move_output_to_job_dir = True
        self._remote_output_dir = remote_output_dir

    def _write_job_scripts_single_simulation(self):
        job_scheduler_commands = self._command_prefix_job_scheduler[self._remote_job_scheduler]
        job_scheduler_prefix = job_scheduler_commands["prefix"]

        cfg_paths = []
        with open(self._dataset_filename) as f:
            for cfg_path in f.readlines():
                cfg_path_stripped = cfg_path.rstrip("\n")

                cfg_paths.append(cfg_path_stripped)

        # Create .set files for the locations of the local job scripts for copying
        # and the remote locations of the job scripts for submitting jobs
        job_script_set_filename = self._dataset_filename.replace(".set", "")

        self._job_script_remote_dataset_filename = job_script_set_filename + "_jobs_remote.set"
        self._job_script_local_dataset_filename = job_script_set_filename + "_jobs.set"

        job_script_local_set = open(self._job_script_local_dataset_filename, "w")
        job_script_remote_set = open(self._job_script_remote_dataset_filename, "w")

        for cfg_path in cfg_paths:
            cfg_file = open(cfg_path)
            cfg_str = cfg_file.read()
            cfg_file.close()

            cfg_filename = os.path.basename(cfg_path)
            cfg_filename_stripped = cfg_filename.replace(".cfg", "")
            cfg_full_dir = os.path.dirname(cfg_path)
            cfg_custom_dir = os.path.split(cfg_full_dir)[1]

            job_script_filename = cfg_filename_stripped + "_job.sh"
            job_script_dir = cfg_full_dir
            job_script_full_path = os.path.join(job_script_dir, job_script_filename)

            with open(job_script_full_path, "w") as job_script:

                # Add job script local location to local .set file
                job_script_local_set.write(job_script_full_path + "\n")

                # Add job script remote location to remote .set file
                remote_job_script_path = self._remote_script_dir
                if self._copy_custom_script_dir:
                    remote_job_script_path = os.path.join(remote_job_script_path, cfg_custom_dir)
                remote_job_script_path = os.path.join(remote_job_script_path, job_script_filename)
                job_script_remote_set.write(remote_job_script_path + "\n")

                # Start writing the job script
                job_script.write("#! /bin/bash\n\n")

                # Write job name
                job_script.write(
                    job_scheduler_prefix + job_scheduler_commands["jobname"] + cfg_filename_stripped + "\n")

                # Write location of stdout and stderr
                remote_job_script_dir = os.path.dirname(remote_job_script_path)
                job_script.write(
                    job_scheduler_prefix + job_scheduler_commands["output"] + os.path.join(remote_job_script_dir,
                                                                                           "stdout.file") + "\n")
                job_script.write(
                    job_scheduler_prefix + job_scheduler_commands["error"] + os.path.join(remote_job_script_dir,
                                                                                          "stderr.file") + "\n")

                # Request an amount of time
                if self._custom_walltime_func:
                    walltime_hours, walltime_minutes, walltime_seconds = self._custom_walltime_func(cfg_str)
                    self._walltime = str(walltime_hours) + ":" + str(walltime_minutes) + ":" + str(walltime_seconds)
                job_script.write(job_scheduler_prefix + job_scheduler_commands["walltime"] + self._walltime + "\n")

                # Request a number of nodes
                nodes_request = job_scheduler_prefix + job_scheduler_commands["nodes"]
                if self._custom_nodes_func:
                    n_nodes = self._custom_nodes_func(cfg_str)
                    job_script.write(nodes_request + str(n_nodes) + "\n")
                else:
                    job_script.write(nodes_request + self._nodes + "\n")

                # Request a number of cores
                cores_request = job_scheduler_prefix + job_scheduler_commands["cores"]
                if self._custom_cores_func:
                    n_cores = self._custom_cores_func(cfg_str)
                    job_script.write(cores_request + str(n_cores) + "\n")
                else:
                    job_script.write(cores_request + self._cores + "\n")

                # Request an amount of memory
                memory_request = job_scheduler_prefix + job_scheduler_commands["memory"]
                if self._custom_memory_func:
                    memory = self._custom_memory_func(cfg_str)
                    job_script.write(memory_request + str(memory) + "G" + "\n")
                else:
                    job_script.write(memory_request + self._memory + "\n")

                # Request a specific partition
                partition_request = job_scheduler_prefix + job_scheduler_commands["partition"]
                if self._custom_partition_func:
                    partition = self._custom_partition_func(cfg_str)
                    job_script.write(partition_request + partition + "\n")
                else:
                    job_script.write(partition_request + self._partition + "\n")

                # Custom commands to execute before program is run
                for custom_command in self._custom_job_commands_before_exec:
                    job_script.write(custom_command + "\n")

                job_script.write("\n\n")
                # Execute program
                remote_cfg_path = self._remote_script_dir
                if self._copy_custom_script_dir:
                    remote_cfg_path = os.path.join(remote_cfg_path, cfg_custom_dir)

                execute_command = self._remote_executable_path + " " + os.path.join(remote_cfg_path, cfg_filename)

                # Add timeout
                if self._timeout_before_walltime_is_set:
                    walltime_in_hours = self._hours_from_walltime_str(self._walltime)

                    timeout_time_in_hours = walltime_in_hours - self._timeout

                    execute_command = "timeout " + str(timeout_time_in_hours) + "h " + execute_command

                # Add custom commands to the execute command
                for command in self._custom_job_commands_during_exec:
                    execute_command += " " + command

                job_script.write(execute_command)

                job_script.write("\n\n")

                # Check if we should move output to job directory
                if self._move_output_to_job_dir:
                    output_files = os.path.join(self._remote_output_dir, cfg_filename_stripped)
                    job_script.write("mv " + output_files + "*" + " " + remote_cfg_path + "\n")

                # Custom commands to execute after program is run
                for custom_command in self._custom_job_commands_after_exec:
                    job_script.write(custom_command + "\n")

        job_script_local_set.close()
        job_script_remote_set.close()

    def _write_job_scripts_multiple_simulations(self):
        job_scheduler_commands = self._command_prefix_job_scheduler[self._remote_job_scheduler]
        job_scheduler_prefix = job_scheduler_commands["prefix"]

        cfg_paths = []
        with open(self._dataset_filename) as f:
            for cfg_path in f.readlines():
                cfg_path_stripped = cfg_path.rstrip("\n")

                cfg_paths.append(cfg_path_stripped)

        # Create .set files for the locations of the local job scripts for copying
        # and the remote locations of the job scripts for submitting jobs
        job_script_set_filename = self._dataset_filename.replace(".set", "")

        self._job_script_remote_dataset_filename = job_script_set_filename + "_jobs_remote.set"
        self._job_script_local_dataset_filename = job_script_set_filename + "_jobs.set"

        job_script_local_set = open(self._job_script_local_dataset_filename, "w")
        job_script_remote_set = open(self._job_script_remote_dataset_filename, "w")

        # Group cfg paths per job
        grouped_cfg_paths = [cfg_paths[n:n + self._n_simulations_per_job] for n in range(0, len(cfg_paths), self._n_simulations_per_job)]
        for i, cfg_group in enumerate(grouped_cfg_paths):

            # Create a job script name stating the group number
            job_script_full_path = self._dataset_filename.replace(".set", "")
            job_script_full_path += "_cfg_group_" + str(i) + "_job.sh"
            job_script_basename = os.path.basename(job_script_full_path)
            job_script_basename_stripped = job_script_basename.replace(".sh", "")

            with open(job_script_full_path, "w") as job_script:
                # Add job script local location to local .set file
                job_script_local_set.write(job_script_full_path + "\n")

                # Add job script remote location to remote .set file
                remote_job_script_path = self._remote_script_dir
                remote_job_script_path = os.path.join(remote_job_script_path, job_script_basename)
                job_script_remote_set.write(remote_job_script_path + "\n")

                # Start writing the job script
                job_script.write("#! /bin/bash\n\n")

                # Write job name
                job_script.write(
                    job_scheduler_prefix + job_scheduler_commands["jobname"] + job_script_basename_stripped + "\n")

                # Write location of stdout and stderr
                remote_job_script_dir = os.path.dirname(remote_job_script_path)
                job_script.write(
                    job_scheduler_prefix + job_scheduler_commands["output"] + os.path.join(remote_job_script_dir,
                                                                                           "stdout_group_" + str(i) + ".file") + "\n")
                job_script.write(
                    job_scheduler_prefix + job_scheduler_commands["error"] + os.path.join(remote_job_script_dir,
                                                                                          "stderr_group_" + str(i) + ".file") + "\n")

                # Resource requesting in multiple simulations per job
                # situation does not support custom functions, since it is not clear
                # what cfg string should determine the resource requirements

                # Request an amount of time
                job_script.write(job_scheduler_prefix + job_scheduler_commands["walltime"] + self._walltime + "\n")

                # Request a number of nodes
                nodes_request = job_scheduler_prefix + job_scheduler_commands["nodes"]
                job_script.write(nodes_request + self._nodes + "\n")

                # Request a number of cores
                cores_request = job_scheduler_prefix + job_scheduler_commands["cores"]
                job_script.write(cores_request + self._cores + "\n")

                # Request an amount of memory
                memory_request = job_scheduler_prefix + job_scheduler_commands["memory"]
                job_script.write(memory_request + self._memory + "\n")

                # Request a specific partition
                partition_request = job_scheduler_prefix + job_scheduler_commands["partition"]
                job_script.write(partition_request + self._partition + "\n")

                # Custom commands to execute before program is run
                for custom_command in self._custom_job_commands_before_exec:
                    job_script.write(custom_command + "\n")

                # Set openMP threads variable to split between n simulations
                omp_num_threads = math.floor(int(self._cores) / len(cfg_group))
                job_script.write("export OMP_NUM_THREADS=" + str(omp_num_threads) + "\n")

                job_script.write("\n\n")

                # Create an execution command for every cfg
                for cfg_path in cfg_group:

                    cfg_basename = os.path.basename(cfg_path)
                    cfg_dirname = os.path.dirname(cfg_path)
                    cfg_custom_dir = os.path.split(cfg_dirname)[1]

                    remote_cfg_path = self._remote_script_dir
                    if self._copy_custom_script_dir:
                        remote_cfg_path = os.path.join(remote_cfg_path, cfg_custom_dir)

                    execute_command = self._remote_executable_path + " " + os.path.join(remote_cfg_path, cfg_basename)

                    # Add timeout
                    if self._timeout_before_walltime_is_set:
                        walltime_in_hours = self._hours_from_walltime_str(self._walltime)

                        timeout_time_in_hours = walltime_in_hours - self._timeout

                        execute_command = "timeout " + str(timeout_time_in_hours) + "h " + execute_command

                    # Add custom commands to the execute command
                    for command in self._custom_job_commands_during_exec:
                        execute_command += " " + command

                    # Run the command in the background
                    execute_command += " &\n"

                    job_script.write(execute_command)

                # Wait for all simulations to have returned
                job_script.write("\nwait\n\n")

                # Check if we should move output to job directory
                if self._move_output_to_job_dir:

                    for cfg_path in cfg_group:
                        cfg_basename = os.path.basename(cfg_path)
                        cfg_basename_stripped = cfg_basename.replace(".cfg", "")
                        cfg_dirname = os.path.dirname(cfg_path)
                        cfg_custom_dir = os.path.split(cfg_dirname)[1]

                        remote_cfg_path = self._remote_script_dir
                        if self._copy_custom_script_dir:
                            remote_cfg_path = os.path.join(remote_cfg_path, cfg_custom_dir)

                        output_files = os.path.join(self._remote_output_dir, cfg_basename_stripped)
                        job_script.write("mv " + output_files + "*" + " " + remote_cfg_path + "\n")

                # Custom commands to execute after program is run
                for custom_command in self._custom_job_commands_after_exec:
                    job_script.write(custom_command + "\n")

        job_script_local_set.close()
        job_script_remote_set.close()

    def write_job_scripts(self):

        if self._n_simulations_per_job == 1:
            self._write_job_scripts_single_simulation()
        else:
            self._write_job_scripts_multiple_simulations()

    def submit_jobs(self):

        # Copy all config and job scripts to remote directory (with or without custom directories
        job_script_local_set = open(self._job_script_local_dataset_filename)
        config_local_set = open(self._dataset_filename)

        scp_arguments = []

        # If I made custom dirs for my job scripts and config scripts then I just need to copy those dirs
        if self._copy_custom_script_dir:

            # Copy the entire directory containing cfg file and job script
            # if we are not grouping simulations in 1 job
            if self._n_simulations_per_job == 1:
                for job_script_path in job_script_local_set.readlines():
                    job_script_path = job_script_path.rstrip("\n")

                    job_script_path = os.path.dirname(job_script_path)

                    scp_arguments.append(job_script_path)
            else:
                # We have to send the job scripts individually
                for job_script_path in job_script_local_set.readlines():
                    job_script_path = job_script_path.rstrip("\n")
                    scp_arguments.append(job_script_path)

                # And send the directories of the cfg files
                for config_path in config_local_set.readlines():
                    config_path = config_path.rstrip("\n")

                    config_path = os.path.dirname(config_path)

                    scp_arguments.append(config_path)
        else:
            # If I did not make custom dirs for my job and config scripts then I need to copy
            # each .cfg and job.sh file individiually

            for job_script_path in job_script_local_set.readlines():
                job_script_path = job_script_path.rstrip("\n")

                scp_arguments.append(job_script_path)

            for config_path in config_local_set.readlines():
                config_path = config_path.rstrip("\n")

                scp_arguments.append(config_path)

        job_script_local_set.close()
        config_local_set.close()

        # Add all local files to scp command
        scp_command = "scp -r "
        for scp_arg in scp_arguments:
            if scp_arg != os.path.abspath("."):
                scp_command += scp_arg + " "

        # Add destination to scp command
        scp_command += self._remote_username + "@" + self._remote_host + ":" + self._remote_script_dir

        print("COPYING ALL JOB AND CONFIG SCRIPTS TO REMOTE")
        subprocess.run(scp_command, shell=True)


        # Submit all job scripts to remote
        remote_job_scripts = []

        job_script_remote_set = open(self._job_script_remote_dataset_filename)

        for remote_job_script in job_script_remote_set.readlines():
            remote_job_script = remote_job_script.rstrip("\n")
            remote_job_scripts.append(remote_job_script)

        # Build the ssh command to submit all job scripts in 1 command
        ssh_command = "ssh " + self._remote_username + "@" + self._remote_host
        job_submit_command = self._command_prefix_job_scheduler[self._remote_job_scheduler]["submit"]
        for i, remote_job_script in enumerate(remote_job_scripts):
            if i == 0:
                ssh_command += " '" + job_submit_command + " " + remote_job_script + ";"
            else:
                ssh_command += " " + job_submit_command + " " + remote_job_script + ";"
        ssh_command += "'"

        print("SUBMITTING ALL JOBS TO THE REMOTE HOST")

        subprocess.run(ssh_command, shell=True)





class RemoteRunner:

    def __init__(self, remote_username, remote_host, remote_path_to_executable, local_path_to_dataset, remote_path_to_script_destination, copy_scripts_with_custom_dir=False):
        print("THIS NEEDS TO BE EXECUTED FROM A TERMINAL")
        self._remote_host = remote_host
        self._remote_username = remote_username

        self._dataset_filename = self._set_dataset_filename(local_path_to_dataset)
        self._job_script_local_dataset_filename = ""
        self._job_script_remote_dataset_filename = ""
        self._remote_executable_path = remote_path_to_executable

        # Sets where the job scripts and config files will be copied to on the remote host
        # before submitting the jobs.
        self._remote_script_dir = remote_path_to_script_destination

        # If custom directories were made for each config you can set "copy_scripts_with_custom_dir" to True
        # so that these custom directories will be copied to the remote_dir as well.
        self._copy_custom_script_dir = copy_scripts_with_custom_dir

        self._custom_job_commands_before_exec = []
        self._custom_job_commands_during_exec = []
        self._custom_job_commands_after_exec = []
        self._custom_walltime_func = None
        self._custom_cores_func = None

        self._walltime = "24:00:00"
        self._walltime_set = False
        self._cores = "8"

        self._n_simulations_per_job = 1

        self._move_output_to_job_dir = False
        self._remote_output_dir = ""

        self._timeout = -1.0
        self._timeout_before_walltime_is_set = False

        self._remote_runner_script_name = ""

    def _hours_from_walltime_str(self, walltime_str):

        split = walltime_str.split(":")

        hours = float(split[0])
        minutes = float(split[1])
        seconds = float(split[2])

        return hours + (1.0 / 60.0) * minutes + (1.0 / 3600.0) * seconds

    def _set_dataset_filename(self, path_to_dataset):

        dataset_filename = path_to_dataset

        if path_to_dataset[0] == "~":
            dataset_filename = os.path.expanduser(path_to_dataset)
        dataset_filename = os.path.abspath(dataset_filename)

        if ".set" not in dataset_filename:
            dataset_filename = dataset_filename + ".set"

        return dataset_filename

    def set_n_simulations_per_job(self, n):
        """
        Number of simulations to run per job.
        This will also change the number of openMP threads
        to be split between simulations.
        """

        self._n_simulations_per_job = n

    def set_walltime_from_cfg_str_func(self, walltime_func):
        self._custom_walltime_func = walltime_func
        self._walltime_set = True

    def set_walltime(self, hours=0, minutes=0, seconds=0):
        self._walltime = str(hours) + ":" + str(minutes) + ":" + str(seconds)
        self._walltime_set = True

    def set_timeout_before_walltime(self, hours=0, minutes=0, seconds=0):
        self._timeout_before_walltime_is_set = True

        total_hours = hours + (1.0 / 60.0) * minutes + (1.0 / 3600.0) * seconds

        self._timeout = total_hours

    def set_cores_from_cfg_str_func(self, cores_func):
        self._custom_cores_func = cores_func

    def set_cores(self, n_cores):
        self._cores = "%.0i" % n_cores

    def add_custom_command_to_job_before_exec(self, custom_command):
        self._custom_job_commands_before_exec.append(custom_command)

    def add_custom_command_to_job_during_exec(self, custom_command):
        self._custom_job_commands_during_exec.append(custom_command)

    def add_custom_command_to_job_after_exec(self, custom_command):
        self._custom_job_commands_after_exec.append(custom_command)

    def move_output_on_remote_to_job_script_dir(self, remote_output_dir):
        """
        Calling this function will add an extra line in the job scripts after
        execution of the program. This extra line takes care of moving the output
        from the remote output directory (if you know it, e.g. /scratch) to the directory of the job script on the remote host.
        """
        self._move_output_to_job_dir = True
        self._remote_output_dir = remote_output_dir

    def _write_job_scripts_single_simulation(self):

        cfg_paths = []
        with open(self._dataset_filename) as f:
            for cfg_path in f.readlines():
                cfg_path_stripped = cfg_path.rstrip("\n")

                cfg_paths.append(cfg_path_stripped)

        # Create .set files for the locations of the local job scripts for copying
        # and the remote locations of the job scripts for submitting jobs
        job_script_set_filename = self._dataset_filename.replace(".set", "")

        self._job_script_remote_dataset_filename = job_script_set_filename + "_jobs_remote.set"
        self._job_script_local_dataset_filename = job_script_set_filename + "_jobs.set"

        job_script_local_set = open(self._job_script_local_dataset_filename, "w")
        job_script_remote_set = open(self._job_script_remote_dataset_filename, "w")

        for cfg_path in cfg_paths:
            cfg_file = open(cfg_path)
            cfg_str = cfg_file.read()
            cfg_file.close()

            cfg_filename = os.path.basename(cfg_path)
            cfg_filename_stripped = cfg_filename.replace(".cfg", "")
            cfg_full_dir = os.path.dirname(cfg_path)
            cfg_custom_dir = os.path.split(cfg_full_dir)[1]

            job_script_filename = cfg_filename_stripped + "_job.sh"
            job_script_dir = cfg_full_dir
            job_script_full_path = os.path.join(job_script_dir, job_script_filename)

            with open(job_script_full_path, "w") as job_script:

                # Add job script local location to local .set file
                job_script_local_set.write(job_script_full_path + "\n")

                # Add job script remote location to remote .set file
                remote_job_script_path = self._remote_script_dir
                if self._copy_custom_script_dir:
                    remote_job_script_path = os.path.join(remote_job_script_path, cfg_custom_dir)
                remote_job_script_path = os.path.join(remote_job_script_path, job_script_filename)
                job_script_remote_set.write(remote_job_script_path + "\n")

                # Start writing the job script
                job_script.write("#! /bin/bash\n\n")

                # Write job name
                job_script.write("#Job Name: " + cfg_filename_stripped + "\n")


                # Custom commands to execute before program is run
                for custom_command in self._custom_job_commands_before_exec:
                    job_script.write(custom_command + "\n")

                # Set openMP threads variable to split between n simulations
                omp_num_threads = math.floor(int(self._cores))
                job_script.write("export OMP_NUM_THREADS=" + str(omp_num_threads) + "\n")


                job_script.write("\n\n")
                # Execute program
                remote_cfg_path = self._remote_script_dir
                if self._copy_custom_script_dir:
                    remote_cfg_path = os.path.join(remote_cfg_path, cfg_custom_dir)

                execute_command = self._remote_executable_path + " " + os.path.join(remote_cfg_path, cfg_filename)

                # Add timeout
                if self._walltime_set:
                    walltime_in_hours = self._hours_from_walltime_str(self._walltime)

                    if self._timeout_before_walltime_is_set:
                        walltime_in_hours = walltime_in_hours - self._timeout

                    execute_command = "timeout " + str(walltime_in_hours) + "h " + execute_command

                # Add custom commands to the execute command
                for command in self._custom_job_commands_during_exec:
                    execute_command += " " + command

                # Run the command in the background
                execute_command += " &\n"

                job_script.write(execute_command)

                # Wait for all simulations to have returned
                job_script.write("\nwait\n\n")

                # Check if we should move output to job directory
                if self._move_output_to_job_dir:
                    output_files = os.path.join(self._remote_output_dir, cfg_filename_stripped)
                    job_script.write("echo " + output_files + "*" + " | xargs mv -t " + remote_cfg_path + "\n")

                # Custom commands to execute after program is run
                for custom_command in self._custom_job_commands_after_exec:
                    job_script.write(custom_command + "\n")

        job_script_local_set.close()
        job_script_remote_set.close()

    def _write_job_scripts_multiple_simulations(self):
        cfg_paths = []
        with open(self._dataset_filename) as f:
            for cfg_path in f.readlines():
                cfg_path_stripped = cfg_path.rstrip("\n")

                cfg_paths.append(cfg_path_stripped)

        # Create .set files for the locations of the local job scripts for copying
        # and the remote locations of the job scripts for submitting jobs
        job_script_set_filename = self._dataset_filename.replace(".set", "")

        self._job_script_remote_dataset_filename = job_script_set_filename + "_jobs_remote.set"
        self._job_script_local_dataset_filename = job_script_set_filename + "_jobs.set"

        job_script_local_set = open(self._job_script_local_dataset_filename, "w")
        job_script_remote_set = open(self._job_script_remote_dataset_filename, "w")

        # Group cfg paths per job
        grouped_cfg_paths = [cfg_paths[n:n + self._n_simulations_per_job] for n in range(0, len(cfg_paths), self._n_simulations_per_job)]
        for i, cfg_group in enumerate(grouped_cfg_paths):

            # Create a job script name stating the group number
            job_script_full_path = self._dataset_filename.replace(".set", "")
            job_script_full_path += "_cfg_group_" + str(i) + "_job.sh"
            job_script_basename = os.path.basename(job_script_full_path)
            job_script_basename_stripped = job_script_basename.replace(".sh", "")

            with open(job_script_full_path, "w") as job_script:
                # Add job script local location to local .set file
                job_script_local_set.write(job_script_full_path + "\n")

                # Add job script remote location to remote .set file
                remote_job_script_path = self._remote_script_dir
                remote_job_script_path = os.path.join(remote_job_script_path, job_script_basename)
                job_script_remote_set.write(remote_job_script_path + "\n")

                # Start writing the job script
                job_script.write("#! /bin/bash\n\n")

                # Write job name
                job_script.write("#Job Name: " + job_script_basename_stripped + "\n")

                # Resource requesting in multiple simulations per job
                # situation does not support custom functions, since it is not clear
                # what cfg string should determine the resource requirements

                # Custom commands to execute before program is run
                for custom_command in self._custom_job_commands_before_exec:
                    job_script.write(custom_command + "\n")

                # Set openMP threads variable to split between n simulations
                omp_num_threads = math.floor(int(self._cores) / len(cfg_group))
                job_script.write("export OMP_NUM_THREADS=" + str(omp_num_threads) + "\n")

                job_script.write("\n\n")

                # Create an execution command for every cfg
                for cfg_path in cfg_group:

                    cfg_basename = os.path.basename(cfg_path)
                    cfg_dirname = os.path.dirname(cfg_path)
                    cfg_custom_dir = os.path.split(cfg_dirname)[1]

                    remote_cfg_path = self._remote_script_dir
                    if self._copy_custom_script_dir:
                        remote_cfg_path = os.path.join(remote_cfg_path, cfg_custom_dir)

                    execute_command = self._remote_executable_path + " " + os.path.join(remote_cfg_path, cfg_basename)


                    # Add timeout
                    if self._walltime_set:
                        walltime_in_hours = self._hours_from_walltime_str(self._walltime)

                        if self._timeout_before_walltime_is_set:
                            walltime_in_hours = walltime_in_hours - self._timeout

                        execute_command = "timeout " + str(walltime_in_hours) + "h " + execute_command

                    # Add custom commands to the execute command
                    for command in self._custom_job_commands_during_exec:
                        execute_command += " " + command

                    # Run the command in the background
                    execute_command += " &\n"

                    job_script.write(execute_command)

                # Wait for all simulations to have returned
                job_script.write("\nwait\n\n")

                # Check if we should move output to job directory
                if self._move_output_to_job_dir:

                    for cfg_path in cfg_group:
                        cfg_basename = os.path.basename(cfg_path)
                        cfg_basename_stripped = cfg_basename.replace(".cfg", "")
                        cfg_dirname = os.path.dirname(cfg_path)
                        cfg_custom_dir = os.path.split(cfg_dirname)[1]

                        remote_cfg_path = self._remote_script_dir
                        if self._copy_custom_script_dir:
                            remote_cfg_path = os.path.join(remote_cfg_path, cfg_custom_dir)

                        output_files = os.path.join(self._remote_output_dir, cfg_basename_stripped)
                        job_script.write("echo " + output_files + "*" + " | xargs mv -t " + remote_cfg_path + "\n")
                # Custom commands to execute after program is run
                for custom_command in self._custom_job_commands_after_exec:
                    job_script.write(custom_command + "\n")

        job_script_local_set.close()
        job_script_remote_set.close()

    def write_job_scripts(self):

        if self._n_simulations_per_job == 1:
            self._write_job_scripts_single_simulation()
        else:
            self._write_job_scripts_multiple_simulations()


        self._create_remote_job_runner_script()

    def submit_jobs(self):

        # Copy all config and job scripts to remote directory (with or without custom directories
        job_script_local_set = open(self._job_script_local_dataset_filename)
        config_local_set = open(self._dataset_filename)

        scp_arguments = []

        # If I made custom dirs for my job scripts and config scripts then I just need to copy those dirs
        if self._copy_custom_script_dir:

            # Copy the entire directory containing cfg file and job script
            # if we are not grouping simulations in 1 job
            if self._n_simulations_per_job == 1:
                for job_script_path in job_script_local_set.readlines():
                    job_script_path = job_script_path.rstrip("\n")

                    job_script_path = os.path.dirname(job_script_path)

                    scp_arguments.append(job_script_path)
            else:
                # We have to send the job scripts individually
                for job_script_path in job_script_local_set.readlines():
                    job_script_path = job_script_path.rstrip("\n")
                    scp_arguments.append(job_script_path)

                # And send the directories of the cfg files
                for config_path in config_local_set.readlines():
                    config_path = config_path.rstrip("\n")

                    config_path = os.path.dirname(config_path)

                    scp_arguments.append(config_path)
        else:
            # If I did not make custom dirs for my job and config scripts then I need to copy
            # each .cfg and job.sh file individiually

            for job_script_path in job_script_local_set.readlines():
                job_script_path = job_script_path.rstrip("\n")

                scp_arguments.append(job_script_path)

            for config_path in config_local_set.readlines():
                config_path = config_path.rstrip("\n")

                scp_arguments.append(config_path)

        job_script_local_set.close()
        config_local_set.close()

        # Add all local files to scp command
        scp_command = "scp -r "
        for scp_arg in scp_arguments:
            if scp_arg != os.path.abspath("."):
                scp_command += scp_arg + " "

        scp_command += self._remote_runner_script_name + " "

        # Add destination to scp command
        scp_command += self._remote_username + "@" + self._remote_host + ":" + self._remote_script_dir

        print("COPYING ALL JOB AND CONFIG SCRIPTS TO REMOTE")
        subprocess.run(scp_command, shell=True)


        # Submit all job scripts to remote
        remote_job_scripts = []

        job_script_remote_set = open(self._job_script_remote_dataset_filename)

        for remote_job_script in job_script_remote_set.readlines():
            remote_job_script = remote_job_script.rstrip("\n")
            remote_job_scripts.append(remote_job_script)

        # Build the ssh command to submit all job scripts in 1 command
        remote_runner_script_name_base = os.path.basename(self._remote_runner_script_name)

        job_submit_command = " 'nohup sh " + os.path.join(self._remote_script_dir, remote_runner_script_name_base) + " > /dev/null 2>&1 &'"
        ssh_command = "ssh " + self._remote_username + "@" + self._remote_host + job_submit_command

        print("SUBMITTING ALL JOBS TO THE REMOTE HOST")
        subprocess.run(ssh_command, shell=True)


    def _create_remote_job_runner_script(self):

        """
        Creates a script which calls each job script sequentially with waits in between
        """

        # Get all the locations of the remote job scripts
        remote_job_scripts = []

        job_script_remote_set = open(self._job_script_remote_dataset_filename)

        for remote_job_script in job_script_remote_set.readlines():
            remote_job_script = remote_job_script.rstrip("\n")
            remote_job_scripts.append(remote_job_script)

        remote_runner_script_name = self._dataset_filename.replace(".set", "") + "_remote_run.sh"


        remote_runner_script = open(remote_runner_script_name, "w")

        for remote_job_script in remote_job_scripts:
            remote_runner_script.write("sh " + remote_job_script + "\n")
            remote_runner_script.write("wait\n")

        remote_runner_script.close()

        self._remote_runner_script_name = remote_runner_script_name




