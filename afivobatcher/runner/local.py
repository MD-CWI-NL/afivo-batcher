from multiprocessing import Pool
import subprocess
import os

dataset = os.path.abspath("../../examples/afivo-streamer/initial_seed_variation")

cfg_files = []
with open(dataset + ".set") as f:
    for line in f.readlines():
        cfg_files.append(line.rstrip("\n"))


def run_command(cfg_path):
    base_command = '/home/andy/Documents/PhD_CWI/afivo-streamer/programs/standard_2d/streamer'
    command = base_command + " " + cfg_path
    subprocess.Popen(command, shell=True)


pool = Pool()
pool.map(run_command, cfg_files)


