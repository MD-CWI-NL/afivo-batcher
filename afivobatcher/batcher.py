from afivobatcher.utils import python_list_to_afivo_cfg_str, python_bool_to_afivo_cfg_str, get_absolute_paths

from decimal import Decimal
import os
import itertools
import numpy as np


# Override this function for your custom names
_default_filename_idx = 0
def default_produce_filename(cfg_string):
    """
    Function to generate filenames to be used to name config files and
    output files. Output files are only named using this function if
    only a directory is given in the output%name afivo variable.
    """
    global _default_filename_idx

    filename = "default_name_" + str(_default_filename_idx)
    _default_filename_idx += 1

    return filename


# Override this function for your custom directories
def default_produce_dirname(cfg_string):
    """
    Function to generate directory names where the config files will be placed in.
    """
    return "."


class AfivoBatch:

    def __init__(self, dataset_name, produce_filename_func=default_produce_filename, produce_dirname_func=default_produce_dirname):

        self.dataset_name = dataset_name
        self.produce_filename_func = produce_filename_func
        self.produce_dirname_func = produce_dirname_func

        self.general_var_dict = {}
        self.general_var_batch = []

        self.var_category_dict = {}

        self._use_produce_name_functions = False

    def set_non_list_var(self, var, val, user_dict=None, user_batch=None, var_category=""):

        if user_dict is None:
            user_dict = self.general_var_dict

        if user_batch is None:
            user_batch = self.general_var_batch

        if type(val) == list:
            user_dict[var] = val
            user_batch.append(var)
        else:
            user_dict[var] = val

        if var_category:
            self.var_category_dict[var] = var_category

    def set_list_var(self, var, val, user_dict=None, user_batch=None, var_category=""):

        if user_dict is None:
            user_dict = self.general_var_dict

        if user_batch is None:
            user_batch = self.general_var_batch

        if type(val[0]) == list:
            user_dict[var] = val
            user_batch.append(var)
        else:
            user_dict[var] = val

        if var_category:
            self.var_category_dict[var] = var_category

    def write_var_dict_to_afivo_cfg_str(self, var_dict, var_batch_vars):

        cfg_batch_strings = []
        cfg_user_str = ""
        line_prefix = ""

        if len(var_dict) > 0:

            for var, val in var_dict.items():

                if var in self.var_category_dict:
                    line_prefix = self.var_category_dict[var] + "%"

                if var not in var_batch_vars:
                    cfg_user_str += self._write_key_value_pair_to_afivo_cfg_str(var, val, line_prefix)
                else:

                    if len(cfg_batch_strings) > 0:
                        # If there are already batch strings, we need to combine every batch string with every batch value
                        #  of this variable which gives us an n x m list with n the amount of batch strings already present
                        # and m the amount of values in current batch var
                        cfg_batch_strings_replace = []
                        for batch_val in val:
                            for batch_str in cfg_batch_strings:
                                cfg_batch_strings_replace.append(
                                    batch_str + self._write_key_value_pair_to_afivo_cfg_str(var, batch_val,
                                                                                            line_prefix))

                        cfg_batch_strings = cfg_batch_strings_replace
                    else:
                        # If there are no batch strings yet, just make a string for every batch value of this variable and put it in a list
                        for batch_val in val:
                            cfg_batch_strings.append(
                                self._write_key_value_pair_to_afivo_cfg_str(var, batch_val, line_prefix))

        if len(cfg_batch_strings) > 0:
            cfg_user_strings = [cfg_user_str + batch_str + "\n\n" for batch_str in cfg_batch_strings]
        else:
            # If we add \n\n after every cfg_str then we will also do so for empty strings
            # creating a lot of whitespace
            if cfg_user_str:
                cfg_user_str += "\n\n"
            cfg_user_strings = [cfg_user_str]

        return cfg_user_strings

    def write_configs(self):

        cfg_strs = self.write_var_dict_to_afivo_cfg_str(self.general_var_dict, self.general_var_batch)

        dataset_file = open(self.dataset_name + ".set", "w")

        for cfg_str in cfg_strs:
            dir_name = self.produce_dirname_func(cfg_str)
            file_name = self.produce_filename_func(cfg_str)

            with open(dir_name + os.path.sep + file_name + ".cfg", "w") as f:
                f.write(cfg_str)

            # Write config path to a dataset file
            dataset_file.write(dir_name + os.path.sep + file_name + ".cfg" + "\n")

        dataset_file.close()

    # Functionality for derived classes to implement default variable values
    def _write_default_dict_to_afivo_cfg_str(self, default_dict, user_dict, category_of_dict=""):

        cfg_str = ""
        line_prefix = ""
        n_written_default_vals = 0
        if len(default_dict) > 0:
            if category_of_dict:
                line_prefix = category_of_dict + "%"

            for key, val in default_dict.items():
                if key not in user_dict:
                    cfg_str += self._write_key_value_pair_to_afivo_cfg_str(key, val, line_prefix)
                    n_written_default_vals += 1

            if n_written_default_vals > 0:
                cfg_str += "\n\n"

        return cfg_str

    def _write_key_value_pair_to_afivo_cfg_str(self, key, val, line_prefix):

        cfg_str = line_prefix + key + " = " + self._write_python_value_to_afivo_cfg_str(val) + "\n"

        return cfg_str

    def _write_python_value_to_afivo_cfg_str(self, val):

        cfg_str = ""

        if type(val) == bool:
            cfg_str = python_bool_to_afivo_cfg_str(val)
        elif type(val) == list:
            cfg_str = python_list_to_afivo_cfg_str(val)
        elif isinstance(val, (np.floating, float)):
            # TODO: write big floats ex. 1000 as 1e3 (scientific notation)
            cfg_str = '%E' % Decimal(val)
        elif type(val) == int:
            cfg_str = str(val)
        elif type(val) == str:
            cfg_str = "'" + val + "'"
        else:
            print("no type found: ", type(val))

        return cfg_str

    def _get_all_combinations_of_cfg_user_strs(self, cfg_user_strs):

        non_empty_cfg_user_strs = [user_strs for user_strs in cfg_user_strs if user_strs]

        all_combinations_of_cfg_user_strs = list(itertools.product(*non_empty_cfg_user_strs))

        return all_combinations_of_cfg_user_strs

    def _is_filename_in_cfg(self, var_name, cfg_str_combination):
        """
        Checks if the output variable "var_name" contains a filename or only a directory.
        """

        file_name_in_output_name = True
        output_name_val = ""

        for cfg_var in cfg_str_combination:
            # If there is no filename present in var_name then we
            # want to add the generated filename to it
            if var_name + " = " in cfg_var:
                split_cfg_var = cfg_var.split("\n")
                for elem in split_cfg_var:
                    if var_name + " = " in elem:
                        output_name_val = elem
                        if elem[-2] == "/":  # -1 should be '
                            file_name_in_output_name = False
                        break

        return file_name_in_output_name, output_name_val


class AfivoStreamerBatch(AfivoBatch):

    def __init__(self, dataset_name, n_dim=2, produce_filename_func=default_produce_filename, produce_dirname_func=default_produce_dirname, enable_circuit=False, enable_preion_channels=False):

        AfivoBatch.__init__(self, dataset_name=dataset_name, produce_filename_func=produce_filename_func, produce_dirname_func=produce_dirname_func)
        self.n_dim = n_dim
        self.enable_circuit = enable_circuit
        self.enable_preion_channels = enable_preion_channels

        ############### DEFAULT DICTIONARIES #########################
        # No Category
        self.default_field_vals = {}
        # No Category
        self.default_refinement_vals = {}
        # No Category
        self.default_density_vals = {}
        # No Category
        self.default_seed_vals = {}
        # No Category
        self.default_file_vals = {}
        # No Category
        self.default_time_vals = {}
        # No Category
        self.default_grid_vals = {}
        # Category: lineout
        self.default_lineout_vals = {}
        # Category: photoi
        self.default_photoi_vals = {}
        # Category: photoi_helmh
        self.default_photoi_helmh_vals = {}
        # Category: photoi_mc
        self.default_photoi_mc_vals = {}
        # Category: plane
        self.default_plane_vals = {}
        # Category: fixes
        self.default_fixes_vals = {}
        # Category: gas
        self.default_gas_vals = {}
        # Category: output
        self.default_output_vals = {}
        # Category: datfile
        self.default_datfile_vals = {}
        # Category: table_data
        self.default_table_data_vals = {}
        # Category: input_data
        self.default_input_data_vals = {}
        # Category: field_maxima
        self.default_field_maxima_vals = {}
        # Category: silo
        self.default_silo_vals = {}
        # No Category
        self.default_circuit_vals = {}
        # No Category
        self.default_preion_channels_vals = {}

        self._create_default_dicts()
        ##############################################################


        ##################### USER DICTIONARIES #####################
        self.user_general_vals = {}
        self.user_general_batch_vars = []

        self.user_field_vals = {}
        self.user_field_batch_vars = []

        self.user_refinement_vals = {}
        self.user_refinement_batch_vars = []

        # I want every seed to have its own dict:
        # Every seed adds a dictionary with 2 variables:
        ## seed_dict = {"values":{}, "batch_vars":[]}
        ## "values":
        #       {"seed_charge_type": ,
        #        "seed_density": ,
        #        "seed_falloff": ,
        #        "seed_rel_r0": [],
        #        "seed_rel_r1": [],
        #        "seed_width": }
        self.user_seeds = []

        self.user_density_vals = {}
        self.user_density_batch_vars = []

        self.user_file_vals = {}
        self.user_file_batch_vars = []

        self.user_time_vals = {}
        self.user_time_batch_vars = []

        self.user_simulation_vals = {}
        self.user_simulation_batch_vars = []

        self.user_grid_vals = {}
        self.user_grid_batch_vars = []

        self.user_lineout_vals = {}
        self.user_lineout_batch_vars = []

        self.user_photoi_vals = {}
        self.user_photoi_batch_vars = []

        self.user_photoi_helmh_vals = {}
        self.user_photoi_helmh_batch_vars = []

        self.user_photoi_mc_vals = {}
        self.user_photoi_mc_batch_vars = []

        self.user_plane_vals = {}
        self.user_plane_batch_vars = []

        self.user_table_data_vals = {}
        self.user_table_data_batch_vars = []

        self.user_input_data_vals = {}
        self.user_input_data_batch_vars = []

        self.user_gas_vals = {}
        self.user_gas_batch_vars = []

        self.user_fixes_vals = {}
        self.user_fixes_batch_vars = []

        self.user_output_vals = {}
        self.user_output_batch_vars = []

        self.user_datfile_vals = {}
        self.user_datfile_batch_vars = []

        self.user_field_maxima_vals = {}
        self.user_field_maxima_batch_vars = []

        self.user_silo_vals = {}
        self.user_silo_batch_vars = []

        self.user_circuit_vals = {}
        self.user_circuit_batch_vars = []

        self.user_preion_channels_vals = {}
        self.user_preion_channels_batch_vars = []
        ################################################################

    def set_field_amplitude(self, val_float):
        """
        The applied electric field (vertical direction).
        """

        self.set_non_list_var("field_amplitude", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_background(self, val_float_list):
        """
         Add this uniform background field (V/m).
        """

        self.set_list_var("field_background", val_float_list, self.user_field_vals, self.user_field_batch_vars)

    def set_field_bc_type(self, val_string):
        """
        Type of boundary condition to use (homogeneous or point_charge).
        """

        self.set_non_list_var('field_bc_type', val_string, self.user_field_vals, self.user_field_batch_vars)

    def set_field_decay_time(self, val_float):
        """
        Decay time of background field.
        """

        self.set_non_list_var("field_decay_time", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_lin_deriv(self, val_float):
        """
        Linear derivative of background field.
        """

        self.set_non_list_var("field_lin_deriv", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_mod_t0(self, val_float):
        """
        Start modifying the vertical background field after this time.
        """

        self.set_non_list_var("field_mod_t0", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_mod_t1(self, val_float):
        """
        Stop modifying the vertical background field after this time.
        """

        self.set_non_list_var("field_mod_t1", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_point_charge(self, val_float):
        """
        Charge (in C) of point charge.
        """

        self.set_non_list_var("field_point_charge", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_point_r0(self, val_float_list):
        """
        Relative position of point charge (outside domain).
        """

        self.set_list_var("field_point_r0", val_float_list, self.user_field_vals, self.user_field_batch_vars)

    def set_field_sin_amplitude(self, val_float):
        """
        Amplitude of sinusoidal modification.
        """

        self.set_non_list_var("field_sin_amplitude", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_sin_freq(self, val_float):
        """
        Frequency (Hz) of sinusoidal modification.
        """

        self.set_non_list_var("field_sin_freq", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_stability_search(self, val_bool):
        """
        If true, enable mode to search stability field.
        """

        self.set_non_list_var("field_stability_search", val_bool, self.user_field_vals, self.user_field_batch_vars)

    def set_field_stability_threshold(self, val_float):
        """
        Use location of maximal field if above this threshold (V/m).
        """

        self.set_non_list_var("field_stability_threshold", val_float, self.user_field_vals,
                               self.user_field_batch_vars)

    def set_field_stability_zmax(self, val_float):
        """
        At this relative position the background field will be zero.
        """

        self.set_non_list_var("field_stability_zmax", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_field_stability_zmin(self, val_float):
        """
        Start lowering background field above this relative position.
        """

        self.set_non_list_var("field_stability_zmin", val_float, self.user_field_vals, self.user_field_batch_vars)

    def set_derefine_cphi(self, val_float):
        """
        Allow derefinement if the curvature in phi is smaller than this value.
        """

        self.set_non_list_var("derefine_cphi", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_derefine_dx(self, val_float):
        """
        Only derefine if grid spacing if smaller than this value.
        """

        self.set_non_list_var("derefine_dx", val_float, self.user_refinement_vals, self.user_refinement_batch_vars)

    def set_refine_adx(self, val_float):
        """
         Refine if alpha*dx is larger than this value.
        """

        self.set_non_list_var("refine_adx", val_float, self.user_refinement_vals, self.user_refinement_batch_vars)

    def set_refine_adx_fac(self, val_float):
        """
         For refinement, use alpha(f * E)/f, where f is this factor.
        """

        self.set_non_list_var("refine_adx_fac", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_buffer_width(self, val_float):
        """
        The refinement buffer width in cells (around flagged cells).
        """

        self.set_non_list_var("refine_buffer_width", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_cphi(self, val_float):
        """
        Refine if the curvature in phi is larger than this value.
        """

        self.set_non_list_var("refine_cphi", val_float, self.user_refinement_vals, self.user_refinement_batch_vars)

    def set_refine_init_fac(self, val_float):
        """
        Refine until dx is smaller than this factor times the seed width.
        """

        self.set_non_list_var("refine_init_fac", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_init_time(self, val_float):
        """
        Refine around initial conditions up to this time.
        """

        self.set_non_list_var("refine_init_time", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_max_dx(self, val_float):
        """
        The grid spacing will always be smaller than this value.
        """

        self.set_non_list_var("refine_max_dx", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_min_dens(self, val_float):
        """
        Minimum electron density for adding grid refinement.
        """

        self.set_non_list_var("refine_min_dens", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_min_dx(self, val_float):
        """
        The grid spacing will always be larger than this value.
        """

        self.set_non_list_var("refine_min_dx", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_per_steps(self, val_float):
        """
        The number of steps after which the mesh is updated.
        """

        self.set_non_list_var("refine_per_steps", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_regions_dr(self, val_float_list):
        """
        Refine a region up to this grid spacing.
        """

        self.set_non_list_var("refine_regions_dr", val_float_list, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_regions_rmax(self, val_float_list):
        """
        Maximum coordinate of the refinement regions.
        """

        self.set_list_var("refine_regions_rmax", val_float_list, self.user_refinement_vals,
                           self.user_refinement_batch_vars)

    def set_refine_regions_rmin(self, val_float_list):
        """
        Minimum coordinate of the refinement regions
        """

        self.set_list_var("refine_regions_rmin", val_float_list, self.user_refinement_vals,
                           self.user_refinement_batch_vars)

    def set_refine_regions_tstop(self, val_float):
        """
        Refine regions up to this simulation time.
        """
        self.set_non_list_var("refine_regions_tstop", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_limits_dr(self, val_float):
        """
        Refine regions at most up to this grid spacing.
        """

        self.set_non_list_var("refine_limits_dr", val_float, self.user_refinement_vals,
                               self.user_refinement_batch_vars)

    def set_refine_limits_rmin(self, val_float_list):
        """
        Minimum coordinate of the refinement limits.
        """

        self.set_list_var("refine_limits_rmin", val_float_list, self.user_refinement_vals,
                           self.user_refinement_batch_vars)

    def set_refine_limits_rmax(self, val_float_list):
        """
        Maximum coordinate of the refinement limits.
        """

        self.set_list_var("refine_limits_rmax", val_float_list, self.user_refinement_vals,
                           self.user_refinement_batch_vars)

    def set_background_density(self, val_float):
        """
        The background ion and electron density (1/m3).
        """

        self.set_non_list_var("background_density", val_float, self.user_density_vals,
                               self.user_density_batch_vars)

    def set_stochastic_density(self, val_float):
        """
        Stochastic background density (1/m3).
        """

        self.set_non_list_var("stochastic_density", val_float, self.user_density_vals,
                               self.user_density_batch_vars)

    def set_prolong_density(self, val_string):
        """
        Density prolongation method (limit, linear, linear_cons, sparse, zeroth)
        """

        self.set_non_list_var("prolong_density", val_string, self.user_density_vals, self.user_density_batch_vars)

    def add_seed(self, seed_charge_type_float, seed_density_float, seed_falloff_string, seed_rel_r0_list,
                 seed_rel_r1_list, seed_width_float):
        """
        Add a seed to the simulation.
        """
        # Each seed has:
        #   - charge type
        #   - density
        #   - falloff
        #   - rel r0
        #   - rel r1
        #   - width

        seed = {"values": {}, "batch_vars": []}

        self.set_non_list_var("seed_charge_type", seed_charge_type_float, seed["values"], seed["batch_vars"])
        self.set_non_list_var("seed_density", seed_density_float, seed["values"], seed["batch_vars"])
        self.set_non_list_var("seed_falloff", seed_falloff_string, seed["values"], seed["batch_vars"])
        self.set_list_var("seed_rel_r0", seed_rel_r0_list, seed["values"], seed["batch_vars"])
        self.set_list_var("seed_rel_r1", seed_rel_r1_list, seed["values"], seed["batch_vars"])
        self.set_non_list_var("seed_width", seed_width_float, seed["values"], seed["batch_vars"])

        self.user_seeds.append(seed)

    def set_silo_write(self, val_bool):
        """
        Write silo output.
        """

        self.set_non_list_var("silo_write", val_bool, self.user_file_vals, self.user_file_batch_vars)

    def set_dt_max(self, val_float):
        """
        The maximum timestep (s).
        """

        self.set_non_list_var("dt_max", val_float, self.user_time_vals, self.user_time_batch_vars)

    def set_dt_min(self, val_float):
        """
        The minimum timestep (s).
        """

        self.set_non_list_var("dt_min", val_float, self.user_time_vals, self.user_time_batch_vars)

    def set_end_time(self, val_float):
        """
        The desired endtime (s) of the simulation.
        """

        self.set_non_list_var("end_time", val_float, self.user_time_vals, self.user_time_batch_vars)

    def set_dt_chemistry_nmin(self, val_float):
        """
        Small density for the chemistry time step.
        """

        self.set_non_list_var("dt_chemistry_nmin", val_float, self.user_time_vals, self.user_time_batch_vars)

    def set_dt_safety_factor(self, val_float):
        """
        Safety factor 'k' for the time step 'dt' calculated from stability analysis and chemistry considerations.
        dt_used = k * dt
        """

        self.set_non_list_var("dt_stafety_factor", val_float, self.user_simulation_vals,
                               self.user_simulation_batch_vars)

    def set_multigrid_num_vcycles(self, val_float):
        """
        Number of V-cycles to perform per time step.
        """

        self.set_non_list_var("multigrid_num_vcycles", val_float, self.user_simulation_vals,
                               self.user_simulation_batch_vars)

    def set_box_size(self, val_float):
        """
        The number of grid cells per coordinate in a box.
        """

        self.set_non_list_var("box_size", val_float, self.user_simulation_vals, self.user_simulation_batch_vars)

    def set_use_end_time(self, val_bool):
        """
        Whether end_time is used to end the simulation.
        """

        self.set_non_list_var("use_end_time", val_bool, self.user_simulation_vals, self.user_simulation_batch_vars)

    def set_use_end_streamer_length(self, val_bool):
        """
        Whether end_streamer_length is used to end the simulation
        """

        self.set_non_list_var("use_end_streamer_length", val_bool, self.user_simulation_vals,
                               self.user_simulation_batch_vars)

    def set_end_streamer_length(self, val_float):
        """
        Streamer length at which the simulation will end. (Length defined as the difference in initial and current position of Emax)
        """

        self.set_non_list_var("end_streamer_length", val_float, self.user_simulation_vals,
                               self.user_simulation_batch_vars)

    def set_time_integrator(self, val_string):
        """
        Time integrator (forward_euler, heuns_method, rk2).
        """

        self.set_non_list_var("time_integrator", val_string, self.user_simulation_vals,
                               self.user_simulation_batch_vars)

    def set_restart_from_file(self, val_string):
        """
        If set, restart simulation from a previous .dat file
        """

        absolute_path_val = get_absolute_paths(val_string)

        self.set_non_list_var("restart_from_file", absolute_path_val, self.user_simulation_vals,
                               self.user_simulation_batch_vars)

    def set_memory_limit_GB(self, val_float):
        """
        Memory limit (GB).
        """

        self.set_non_list_var("memory_limit_GB", val_float, self.user_simulation_vals,
                               self.user_simulation_batch_vars)

    def set_rng_seed(self, val_float_list):
        """
        "Seed for random numbers; if all zero, generate randomly.
        """

        self.set_list_var("rng_seed", val_float_list, self.user_simulation_vals, self.user_simulation_batch_vars)

    def set_periodic(self, val_bool_list):
        """
        Whether the domain is periodic (per dimension).
        """

        self.set_list_var("periodic", val_bool_list, self.user_grid_vals, self.user_grid_batch_vars)

    def set_cylindrical(self, val_bool):
        """
        Whether cylindrical coordinates are used. (only in 2D)
        """

        self.set_non_list_var("cylindrical", val_bool, self.user_grid_vals, self.user_grid_batch_vars)

    def set_use_dielectric(self, val_bool):
        """
        Whether a dielectric is used. (experimental)
        """

        self.set_non_list_var("use_dielectric", val_bool, self.user_grid_vals, self.user_grid_batch_vars)

    def set_domain_len(self, val_float_list):
        """
        The length of the domain for every coordinate (m).
        """

        self.set_list_var("domain_len", val_float_list, self.user_grid_vals, self.user_grid_batch_vars)

    def set_coarse_grid_size(self, val_float_list):
        """
        The size of the coarse grid.
        """

        self.set_list_var("coarse_grid_size", val_float_list, self.user_grid_vals, self.user_grid_batch_vars)

    def set_domain_origin(self, val_float_list):
        """
        The origin of the domain (m).
        """

        self.set_list_var("domain_origin", val_float_list, self.user_grid_vals, self.user_grid_batch_vars)

    def set_lineout_npoints(self, val_float):
        """
        Use this many points for lineout data.
        """

        self.set_non_list_var("npoints", val_float, self.user_lineout_vals, self.user_lineout_batch_vars, "lineout")

    def set_lineout_rmax(self, val_float_list):
        """
        Relative position of line maximum coordinate.
        """

        self.set_list_var("rmax", val_float_list, self.user_lineout_vals, self.user_lineout_batch_vars, "lineout")

    def set_lineout_rmin(self, val_float_list):
        """
        Relative position of line minimum coordinate.
        """

        self.set_list_var("rmin", val_float_list, self.user_lineout_vals, self.user_lineout_batch_vars, "lineout")

    def set_lineout_write(self, val_bool):
        """
        Write output along a line.
        """

        self.set_non_list_var("write", val_bool, self.user_lineout_vals, self.user_lineout_batch_vars, "lineout")

    def set_photoi_enabled(self, val_bool):
        """
        Whether photoionization is enabled.
        """

        self.set_non_list_var("enabled", val_bool, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")

    def set_photoi_eta(self, val_float):
        """
        Photoionization efficiency factor, typically around 0.05-0.1.
        """

        self.set_non_list_var("eta", val_float, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")

    def set_photoi_method(self, val_string):
        """
        Which photoionization method to use (helmholtz, montecarlo).
        """

        self.set_non_list_var("method", val_string, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")

    def set_photoi_per_steps(self, val_float):
        """
        Update photoionization every N time step.
        """

        self.set_non_list_var("per_steps", val_float, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")

    def set_photoi_quenching_pressure(self, val_float):
        """
        Photoionization quenching pressure (bar)
        """

        self.set_non_list_var("quenching_pressure", val_float, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")

    def set_photoi_source_type(self, val_string):
        """
        How to compute the photoi. source (Zheleznyak, from_species)
        """

        self.set_non_list_var("source_type", val_string, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")

    def set_photoi_excited_species(self, val_string_list):
        """
        Which excited species to use when photoi%source_type = from_species
        """

        # TODO: validate that this config variable takes a list of strings

        self.set_list_var("excited_species", val_string_list, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")

    def set_photoi_photoemission_time(self, val_float):
        """
        Photoemission time delay in case photoi_source_type is 'from_species'
        """

        # TODO: validate that this config variable takes only 1 float

        self.set_non_list_var("photoemission_time", val_float, self.user_photoi_vals, self.user_photoi_batch_vars, "photoi")


    def set_photoi_helm_author(self, val_string):
        """
        Luque or Bourdon coeffients are used?.
        """

        self.set_non_list_var("author", val_string, self.user_photoi_helmh_vals, self.user_photoi_helmh_batch_vars, "photoi_helmh")

    def set_photoi_helm_coeffs(self, val_float_list):
        """
        Weights corresponding to the lambdas; unit 1/(m bar)^2.
        """

        self.set_list_var("coeffs", val_float_list, self.user_photoi_helmh_vals, self.user_photoi_helmh_batch_vars, "photoi_helmh")

    def set_photoi_helm_lambdas(self, val_float_list):
        """
        Lambdas to use for lpl(phi) - lambda*phi = f; unit 1/(m bar).
        """

        self.set_list_var("lambdas", val_float_list, self.user_photoi_helmh_vals, self.user_photoi_helmh_batch_vars, "photoi_helmh")

    def set_photoi_helm_max_rel_residual(self, val_float):
        """
        Maximum residual relative to max(|rhs|).
        """

        self.set_non_list_var("max_rel_residual", val_float, self.user_photoi_helmh_vals,
                               self.user_photoi_helmh_batch_vars, "photoi_helmh")

    def set_photoi_mc_absorp_fac(self, val_float):
        """
        At which grid spacing photons are absorbed compared to their mean distance.
        """

        self.set_non_list_var("absorp_fac", val_float, self.user_photoi_mc_vals, self.user_photoi_mc_batch_vars, "photoi_mc")

    def set_photoi_mc_const_dx(self, val_bool):
        """
        Whether a constant grid spacing is used for photoionization.
        """

        self.set_non_list_var("const_dx", val_bool, self.user_photoi_mc_vals, self.user_photoi_mc_batch_vars, "photoi_mc")

    def set_photoi_mc_min_dx(self, val_float):
        """
        Minimum grid spacing for photoionization.
        """

        self.set_non_list_var("min_dx", val_float, self.user_photoi_mc_vals, self.user_photoi_mc_batch_vars, "photoi_mc")

    def set_photoi_mc_num_photons(self, val_float):
        """
        Maximum number of discrete photons to use.
        """

        self.set_non_list_var("num_photons", val_float, self.user_photoi_mc_vals, self.user_photoi_mc_batch_vars, "photoi_mc")

    def set_photoi_mc_physical_photons(self, val_bool):
        """
        Whether physical photons are used.
        """

        self.set_non_list_var("physical_photons", val_bool, self.user_photoi_mc_vals,
                               self.user_photoi_mc_batch_vars, "photoi_mc")

    def set_plane_npixels(self, val_float_list):
        """
        Use this many pixels for plane data.
        """

        self.set_list_var("npixels", val_float_list, self.user_plane_vals, self.user_plane_batch_vars, "plane")

    def set_plane_rmax(self, val_float_list):
        """
        Relative position of plane maximum coordinate.
        """

        self.set_list_var("rmax", val_float_list, self.user_plane_vals, self.user_plane_batch_vars, "plane")

    def set_plane_rmin(self, val_float_list):
        """
        Relative position of plane minimum coordinate.
        """

        self.set_list_var("rmin", val_float_list, self.user_plane_vals, self.user_plane_batch_vars, "plane")

    def set_plane_write(self, val_bool):
        """
        Write uniform output in a plane.
        """

        self.set_non_list_var("write", val_bool, self.user_plane_vals, self.user_plane_batch_vars, "plane")

    def set_plane_varname(self, val_string):
        """
        Names of variable to write in a plane.
        """

        self.set_non_list_var("varname", val_string, self.user_plane_vals, self.user_plane_batch_vars, "plane")

    def set_fixes_diffusion_field_limit(self, val_float):
        """
        Disable diffusion parallel to fields above this threshold (V/m).
        """

        self.set_non_list_var("diffusion_field_limit", val_float, self.user_fixes_vals, self.user_fixes_batch_vars, "fixes")

    def set_fixes_drt_limit_flux(self, val_bool):
        """
        Avoid dielectric relaxation time step constraint by limiting flux.
        """

        self.set_non_list_var("drt_limit_flux", val_bool, self.user_fixes_vals, self.user_fixes_batch_vars, "fixes")

    def set_fixes_max_velocity(self, val_float):
        """
        Limit velocities to this value (m/s).
        """

        self.set_non_list_var("max_velocity", val_float, self.user_fixes_vals, self.user_fixes_batch_vars, "fixes")

    def set_gas_components(self, val_string_list):
        """
        Gas component names.
        """

        self.set_list_var("components", val_string_list, self.user_gas_vals, self.user_gas_batch_vars, "gas")

    def set_gas_fractions(self, val_float_list):
        """
        Gas component fractions.
        """

        self.set_list_var("fractions", val_float_list, self.user_gas_vals, self.user_gas_batch_vars, "gas")

    def set_gas_pressure(self, val_float):
        """
        The gas pressure (bar).
        """

        self.set_non_list_var("pressure", val_float, self.user_gas_vals, self.user_gas_batch_vars, "gas")

    def set_gas_temperature(self, val_float):
        """
        The gas temperature (Kelvin).
        """

        self.set_non_list_var("temperature", val_float, self.user_gas_vals, self.user_gas_batch_vars, "gas")

    def set_output_dt(self, val_float):
        """
        Time between writing output.
        """

        self.set_non_list_var("dt", val_float, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_only(self, val_string_list):
        """
        If defined, only output these variables
        """

        self.set_list_var("only", val_string_list, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_name(self, val_string_output_dir="", val_string_output_name="", use_produce_name_functions=False):
        """
        Name for the output files.  output_dir/output_filename.

        Note: This is the only function where you cannot supply more than 1 directory/filename.
        If different filenames are required only supply 1 dirname.
        If also different dirnames are required set "use_produce_name_functions" to True and provide
        the batcher with suitable functions for produce_filename_func and produce_dirname_func.

        If only output_dir is given then output_filename will be generated by the produce_filename_func

        if use_produce_name_functions is set to True then the output_directory and the output_filenames will be
        generated using the functions passed to produce_dirname_func and produce_filename_func respectively.

        """

        if type(val_string_output_dir) == list:
            print("Cannot provide more than 1 output directory")
            print("If output has to be put in different directories based on the simulation")
            print("then set \"use_produce_name_functions\" to True and provide the batcher with a suitable")
            print("produce_dirname_func")
            exit(-1)

        if type(val_string_output_name) == list:
            print("Cannot provide more than 1 filename")
            print("If output has to be named differently based on the simulation")
            print("then do not pass filenames to this function but pass a suitable \"produce_filename_func\" to the batcher")
            exit(-1)

        if not use_produce_name_functions:
            output_dir = val_string_output_dir + os.path.sep if val_string_output_dir[-1] != os.path.sep else val_string_output_dir
            val_string = output_dir + val_string_output_name
            self.set_non_list_var("name", val_string, self.user_output_vals, self.user_output_batch_vars, "output")
        else:
            self.set_non_list_var("name", "REPLACEME", self.user_output_vals, self.user_output_batch_vars, "output")
            self._use_produce_name_functions = use_produce_name_functions

    def set_output_src_decay_time(self, val_float):
        """
        If positive: decay time for source term (s) for time-averaged values.
        """

        self.set_non_list_var("src_decay_time", val_float, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_src_term(self, val_bool):
        """
        Include ionization source term in output.
        """

        self.set_non_list_var("src_term", val_bool, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_status_delay(self, val_float):
        """
        Print status every this many seconds.
        """

        self.set_non_list_var("status_delay", val_float, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_datfile_write(self, val_bool):
        """
        Write binary output files (to resume later).
        """

        self.set_non_list_var("write", val_bool, self.user_datfile_vals, self.user_datfile_batch_vars, "datfile")

    def set_datfile_per_outputs(self, val_float):
        """
        Write binary output files every N outputs.
        """

        self.set_non_list_var("per_outputs", val_float, self.user_datfile_vals, self.user_datfile_batch_vars, "datfile")

    def set_table_data_max_townsend(self, val_float):
        """
        Maximal field (in Td) for the rate coeff. lookup table.
        """

        self.set_non_list_var("max_townsend", val_float, self.user_table_data_vals,
                               self.user_table_data_batch_vars, "table_data")

    def set_table_data_min_townsend(self, val_float):
        """
        Minimal field (in Td) for the rate coeff. lookup table.
        """

        self.set_non_list_var("min_townsend", val_float, self.user_table_data_vals,
                               self.user_table_data_batch_vars, "table_data")

    def set_table_data_size(self, val_float):
        """
        Size of the lookup table for reaction rates.
        """

        self.set_non_list_var("size", val_float, self.user_table_data_vals, self.user_table_data_batch_vars, "table_data")

    def set_input_data_file(self, val_string):
        """
        Input file with transport (and reaction) data.
        """

        absolute_path_val = get_absolute_paths(val_string)

        self.set_non_list_var("file", absolute_path_val, self.user_input_data_vals, self.user_input_data_batch_vars, "input_data")

    def set_input_data_old_style(self, val_bool):
        """
        Use old style transport data (alpha, eta, mu, D vs V/m).
        """

        self.set_non_list_var("old_style", val_bool, self.user_input_data_vals, self.user_input_data_batch_vars, "input_data")

    def set_field_maxima_write(self, val_bool):
        """
        Output electric field maxima and their locations
        """

        self.set_non_list_var("write", val_bool, self.user_field_maxima_vals, self.user_field_maxima_batch_vars, "field_maxima")

    def set_field_maxima_threshold(self, val_float):
        """
        Threshold value (V/m) for electric field maxima
        """

        self.set_non_list_var("threshold", val_float, self.user_field_maxima_vals, self.user_field_maxima_batch_vars, "field_maxima")

    def set_field_maxima_distance(self, val_float):
        """
        Minimal distance (m) between electric field maxima
        """

        self.set_non_list_var("distance", val_float, self.user_field_maxima_vals, self.user_field_maxima_batch_vars, "field_maxima")

    def set_silo_per_outputs(self, val_float):
        """
        Write silo output files every N outputs
        """

        self.set_non_list_var("per_outputs", val_float, self.user_silo_vals, self.user_silo_batch_vars, "silo")
    
    def set_use_circuit_andy(self, val_bool):
        """
        If set use the circuit implementation of Andy.
        """

        self.set_non_list_var("use_circuit_andy", val_bool, self.user_circuit_vals, self.user_circuit_batch_vars)

    def set_use_circuit_R(self, val_bool):
        """
        If set use the circuit implementation of W. Riegler.
        """

        self.set_non_list_var("use_circuit_R", val_bool, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_use_top_electrode_sphere(self, val_bool):
        """
        If set we approximate the top electrode as a sphere for self capacitance calculations (for needle electrodes).
        """

        self.set_non_list_var("use_top_electrode_sphere", val_bool, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_use_bottom_electrode_sphere(self, val_bool):
        """
        If set we approximate the bottom electrode as a sphere for self capacitance calculations (for needle electrodes).
        """        

        self.set_non_list_var("use_bottom_electrode_sphere", val_bool, self.user_circuit_vals, self.user_circuit_batch_vars)

    def set_top_electrode_sphere_radius(self, val_float):
        """
        Determines the radius of the top electrode when approximated as a sphere.
        """

        self.set_non_list_var("top_electrode_sphere_radius", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_bottom_electrode_sphere_radius(self, val_float):
        """
        Determines the radius of the bottom electrode when approximated as a sphere.
        """

        self.set_non_list_var("bottom_electrode_sphere_radius", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_capacitor_capacitance(self, val_float):
        """
        Capacitance of the capacitor used in the RC circuit implementation of Andy/Riegler.
        """

        self.set_non_list_var("capacitor_capacitance", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_resistor(self, val_float):
        """
        Resistor value for the resistor in the RC circuit implementation of Andy/Riegler.
        """

        self.set_non_list_var("resistor", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_gnd_resistor(self, val_float):
        """
        Resistor value for the connection of the bottom plate to ground.
        """

        self.set_non_list_var("gnd_resistor", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_capacitor_initial_voltage_fraction(self, val_float):
        """
        Initial capacitor voltage as a fraction of the applied electric field for RC Andy/Riegler.
        """

        self.set_non_list_var("capacitor_initial_voltage_fraction", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_top_electrode_initial_voltage_fraction(self, val_float):
        """
        Initial top electrode voltage as a fraction of the applied electric field for RC Andy/Riegler.
        """

        self.set_non_list_var("top_electrode_initial_voltage_fraction", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)
    
    def set_n_streamers_circuit(self, val_float):
        """
        Number of streamers we pretend to simulate for power consumption of the circuit.
        """

        self.set_non_list_var("n_streamers_circuit", val_float, self.user_circuit_vals, self.user_circuit_batch_vars)

    def set_species_boundary_conditions(self, val_string):
        """
        Boundary condition for the plasma species. (neumann_zero or dirichlet_zero)
        """

        self.set_non_list_var("species_boundary_condition", val_string, self.user_simulation_vals, self.user_simulation_batch_vars)

    def set_t_field_off(self, val_float):
        """
        Time after which the applied field is turned off.
        """

        self.set_non_list_var("t_field_off", val_float, self.user_preion_channels_vals, self.user_preion_channels_batch_vars)

    def write_configs(self):

        cfg_default_str = "######### DEFAULT #################\n\n"
        cfg_default_str += self._write_default_dicts_to_afivo_cfg_str()

        cfg_user_str = "######### USER #################\n\n"

        cfg_general_var_strs = self.write_var_dict_to_afivo_cfg_str(self.general_var_dict, self.general_var_batch)
        cfg_user_field_strs = self.write_var_dict_to_afivo_cfg_str(self.user_field_vals, self.user_field_batch_vars)
        cfg_user_refinement_strs = self.write_var_dict_to_afivo_cfg_str(self.user_refinement_vals, self.user_refinement_batch_vars)
        cfg_user_density_strs = self.write_var_dict_to_afivo_cfg_str(self.user_density_vals, self.user_density_batch_vars)
        cfg_user_seeds_strs = self._write_user_seeds_to_afivo_cfg_str(self.user_seeds)
        cfg_user_file_strs = self.write_var_dict_to_afivo_cfg_str(self.user_file_vals, self.user_file_batch_vars)
        cfg_user_time_strs = self.write_var_dict_to_afivo_cfg_str(self.user_time_vals, self.user_time_batch_vars)
        cfg_user_simulation_strs = self.write_var_dict_to_afivo_cfg_str(self.user_simulation_vals, self.user_simulation_batch_vars)
        cfg_user_grid_strs = self.write_var_dict_to_afivo_cfg_str(self.user_grid_vals, self.user_grid_batch_vars)
        cfg_user_lineout_strs = self.write_var_dict_to_afivo_cfg_str(self.user_lineout_vals, self.user_lineout_batch_vars)
        cfg_user_photoi_strs = self.write_var_dict_to_afivo_cfg_str(self.user_photoi_vals, self.user_photoi_batch_vars)
        cfg_user_photoi_helm_strs = self.write_var_dict_to_afivo_cfg_str(self.user_photoi_helmh_vals, self.user_photoi_helmh_batch_vars)
        cfg_user_photoi_mc_strs = self.write_var_dict_to_afivo_cfg_str(self.user_photoi_mc_vals, self.user_photoi_mc_batch_vars)
        cfg_user_plane_strs = self.write_var_dict_to_afivo_cfg_str(self.user_plane_vals, self.user_plane_batch_vars)
        cfg_user_fixes_strs = self.write_var_dict_to_afivo_cfg_str(self.user_fixes_vals, self.user_fixes_batch_vars)
        cfg_user_gas_strs = self.write_var_dict_to_afivo_cfg_str(self.user_gas_vals, self.user_gas_batch_vars)
        cfg_user_output_strs = self.write_var_dict_to_afivo_cfg_str(self.user_output_vals, self.user_output_batch_vars)
        cfg_user_datfile_strs = self.write_var_dict_to_afivo_cfg_str(self.user_datfile_vals, self.user_datfile_batch_vars)
        cfg_user_table_data_strs = self.write_var_dict_to_afivo_cfg_str(self.user_table_data_vals, self.user_table_data_batch_vars)
        cfg_user_input_data_strs = self.write_var_dict_to_afivo_cfg_str(self.user_input_data_vals, self.user_input_data_batch_vars)
        cfg_user_field_maxima_strs = self.write_var_dict_to_afivo_cfg_str(self.user_field_maxima_vals, self.user_field_maxima_batch_vars)
        cfg_user_silo_strs = self.write_var_dict_to_afivo_cfg_str(self.user_silo_vals, self.user_silo_batch_vars)

        cfg_user_strs = [cfg_general_var_strs, cfg_user_field_strs, cfg_user_refinement_strs, cfg_user_density_strs, cfg_user_seeds_strs,
                         cfg_user_file_strs, cfg_user_time_strs, cfg_user_simulation_strs, cfg_user_grid_strs, cfg_user_lineout_strs, cfg_user_photoi_strs,
                         cfg_user_photoi_helm_strs, cfg_user_photoi_mc_strs, cfg_user_plane_strs, cfg_user_fixes_strs, cfg_user_gas_strs,
                         cfg_user_output_strs, cfg_user_datfile_strs, cfg_user_table_data_strs, cfg_user_input_data_strs, cfg_user_field_maxima_strs,
                         cfg_user_silo_strs]
        
        cfg_user_circuit_strs = ""
        if self.enable_circuit:
            cfg_user_circuit_strs = self.write_var_dict_to_afivo_cfg_str(self.user_circuit_vals, self.user_circuit_batch_vars)
            cfg_user_strs.append(cfg_user_circuit_strs)

        cfg_user_preion_channels_strs = ""
        if self.enable_preion_channels:
            cfg_user_preion_channels_strs = self.write_var_dict_to_afivo_cfg_str(self.user_preion_channels_vals, self.user_preion_channels_batch_vars)
            cfg_user_strs.append(cfg_user_preion_channels_strs)

        all_combinations_of_cfg_user_strs = self._get_all_combinations_of_cfg_user_strs(cfg_user_strs)

        dataset_file = open(self.dataset_name + ".set", "w")

        for combination in all_combinations_of_cfg_user_strs:
            cfg_str = cfg_user_str

            for cfg_var in combination:
                cfg_str += cfg_var

            cfg_str += cfg_default_str

            file_name_in_output_name, output_name_val = self._is_filename_in_cfg("output%name", combination)

            # Generate custom filenames and directories for the config files
            dir_name = self.produce_dirname_func(cfg_str)
            # Create an absolute path out of the dir_name
            if dir_name[0] == "~":
                dir_name = os.path.expanduser(dir_name)
            dir_name = os.path.abspath(dir_name)

            file_name = self.produce_filename_func(cfg_str)

            if not self._use_produce_name_functions:
                # We add the generated filename from produce_filename_func to output%name
                if not file_name_in_output_name:
                    cfg_str = cfg_str.replace(output_name_val[:-1], output_name_val[:-1] + file_name)
            else:
                cfg_str = cfg_str.replace("REPLACEME", dir_name + os.path.sep + file_name)

            # If the custom directory does not exist, create it.
            if not os.path.exists(dir_name):
                os.mkdir(dir_name)

            with open(dir_name + os.path.sep + file_name + ".cfg", "w") as f:
                f.write(cfg_str)

            # Write config path to a dataset file
            dataset_file.write(dir_name + os.path.sep + file_name + ".cfg" + "\n")

        dataset_file.close()

    ####### WRITING USER DEFINED SEEDS ##################################

    def _write_user_seeds_to_afivo_cfg_str(self, user_seeds):
        cfg_batch_strings = []

        if len(user_seeds) > 0:
            seed_charge_type_strs = self._write_seed_property_to_afivo_cfg_strs("seed_charge_type", user_seeds)
            seed_density_strs = self._write_seed_property_to_afivo_cfg_strs("seed_density", user_seeds)
            seed_falloff_strs = self._write_seed_property_to_afivo_cfg_strs("seed_falloff", user_seeds)
            seed_rel_r0_strs = self._write_seed_property_to_afivo_cfg_strs("seed_rel_r0", user_seeds)
            seed_rel_r1_strs = self._write_seed_property_to_afivo_cfg_strs("seed_rel_r1", user_seeds)
            seed_width_strs = self._write_seed_property_to_afivo_cfg_strs("seed_width", user_seeds)
            seed_strs = [seed_charge_type_strs, seed_density_strs, seed_falloff_strs, seed_rel_r0_strs,
                         seed_rel_r1_strs, seed_width_strs]
            all_combinations_of_seed_strs = list(itertools.product(*seed_strs))

            for combination in all_combinations_of_seed_strs:
                cfg_str = ""

                for cfg_var in combination:
                    cfg_str += cfg_var

                cfg_str += "\n\n"

                cfg_batch_strings.append(cfg_str)

        return cfg_batch_strings

    def _write_seed_property_to_afivo_cfg_strs(self, seed_property, user_seeds):

        seed_property_strs = []

        for seed in user_seeds:

            batch_vars = seed["batch_vars"]

            seed_vals = seed["values"]

            # If this is the first seed
            if len(seed_property_strs) == 0:
                if seed_property not in batch_vars:
                    seed_property_cfg_str = self._write_key_value_pair_to_afivo_cfg_str(seed_property, seed_vals[seed_property], "")

                    # Remove "\n" at end of cfg_str
                    seed_property_cfg_str = seed_property_cfg_str[:-1]

                    seed_property_strs.append(seed_property_cfg_str)
                else:
                    for seed_val in seed_vals[seed_property]:
                        seed_property_cfg_str = self._write_key_value_pair_to_afivo_cfg_str(seed_property, seed_val, "")

                        # Remove "\n" at end of cfg_str
                        seed_property_cfg_str = seed_property_cfg_str[:-1]

                        seed_property_strs.append(seed_property_cfg_str)
            else:

                seed_property_strs_replace = []

                if seed_property not in batch_vars:
                    # Take all already written cfg_strs and append an extra value to them
                    # without adding an extra variable name to the cfg str
                    for seed_property_str in seed_property_strs:

                        new_seed_property_str = seed_property_str + " " + self._write_python_value_to_afivo_cfg_str(seed_vals[seed_property])

                        seed_property_strs_replace.append(new_seed_property_str)
                else:
                    for seed_val in seed_vals[seed_property]:
                        for seed_property_str in seed_property_strs:
                            new_seed_property_str = seed_property_str + " " + self._write_python_value_to_afivo_cfg_str(seed_val)

                            seed_property_strs_replace.append(new_seed_property_str)

                seed_property_strs = seed_property_strs_replace

        seed_property_strs = [seed_property_string + "\n" for seed_property_string in seed_property_strs]

        return seed_property_strs
    #####################################################################

    ###### WRITING DEFAULT AFIVO STREAMER VALUES  #######################

    def _write_default_dicts_to_afivo_cfg_str(self):

        cfg_default_str = ""
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_field_vals, self.user_field_vals)
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_refinement_vals, self.user_refinement_vals)
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_density_vals, self.user_density_vals)
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_file_vals, self.user_file_vals)
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_time_vals, self.user_time_vals)
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_simulation_vals, self.user_simulation_vals)
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_grid_vals, self.user_grid_vals)
        cfg_default_str += self._write_default_seed_dict_to_afivo_cfg_str(self.default_seeds_vals, self.user_seeds)

        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_lineout_vals, self.user_lineout_vals, "lineout")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_photoi_vals, self.user_photoi_vals, "photoi")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_photoi_helmh_vals, self.user_photoi_helmh_vals, "photoi_helmh")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_photoi_mc_vals, self.user_photoi_mc_vals, "photoi_mc")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_plane_vals, self.user_plane_vals, "plane")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_fixes_vals, self.user_fixes_vals, "fixes")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_gas_vals, self.user_gas_vals, "gas")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_output_vals, self.user_output_vals, "output")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_datfile_vals, self.user_datfile_vals, "datfile")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_table_data_vals, self.user_table_data_vals, "table_data")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_input_data_vals, self.user_input_data_vals, "input_data")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_field_maxima_vals, self.user_field_maxima_vals, "field_maxima")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_silo_vals, self.user_silo_vals, "silo")

        if self.enable_circuit:
            cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_circuit_vals, self.user_circuit_vals)

        if self.enable_preion_channels:
            cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_preion_channels_vals, self.user_preion_channels_vals)

        return cfg_default_str

    def _write_default_seed_dict_to_afivo_cfg_str(self, default_seed_list, user_seed_list):

        cfg_str = ""

        if len(user_seed_list) == 0:

            for key, val in default_seed_list[0]["values"].items():
                cfg_str += self._write_key_value_pair_to_afivo_cfg_str(key, val, "")

            cfg_str += "\n\n"

        return cfg_str

    def _create_default_dicts(self):

        # No Category
        self.default_field_vals = {"field_amplitude": 1e6,
                                   "field_background": [0.0] * self.n_dim,
                                   "field_bc_type": "homogeneous",
                                   "field_decay_time": 1e308,
                                   "field_lin_deriv": 0.0,
                                   "field_mod_t0": 1e99,
                                   "field_mod_t1": 1e99,
                                   "field_point_charge": 0.0,
                                   "field_point_r0": [0.0] * (self.n_dim - 1) + [-1.0],
                                   "field_sin_amplitude": 0.0,
                                   "field_sin_freq": 0.0,
                                   "field_stability_search": False,
                                   "field_stability_threshold": 3e6,
                                   "field_stability_zmax": 1.0,
                                   "field_stability_zmin": 0.2}

        # No Category
        self.default_refinement_vals = {"derefine_cphi": 1e99,
                                        "derefine_dx": 1e-4,
                                        "refine_adx": 1.0,
                                        "refine_adx_fac": 1.0,
                                        "refine_buffer_width": 4,
                                        "refine_cphi": 1e99,
                                        "refine_init_fac": 0.25,
                                        "refine_init_time": 1e-8,
                                        "refine_max_dx": 1e-3,
                                        "refine_min_dens": -1e99,
                                        "refine_min_dx": 1e-7,
                                        "refine_per_steps": 2,
                                        "refine_regions_dr": 1e99,
                                        "refine_regions_rmax": [0.0] * self.n_dim,
                                        "refine_regions_rmin": [0.0] * self.n_dim,
                                        "refine_regions_tstop": 1e99,
                                        "refine_limits_dr": 1e99,
                                        "refine_limits_rmin": [0.0] * self.n_dim,
                                        "refine_limits_rmax": [0.0] * self.n_dim}

        # No Category
        self.default_density_vals = {"background_density": 0.0,
                                     "stochastic_density": 0.0,
                                     "prolong_density": "limit"}

        self.default_seeds_vals = [{"values": {"seed_charge_type": 0,
                                               "seed_density": 1e15,
                                               "seed_density2": 1e15,
                                               "seed_falloff": "smoothstep",
                                               "seed_rel_r0": [0.5] * self.n_dim,
                                               "seed_rel_r1": [0.5] * self.n_dim,
                                               "seed_width": 0.25e-3}}]

        # No Category
        self.default_file_vals = {"silo_write": True}

        # No Category
        self.default_time_vals = {"dt_max": 1e-11,
                                  "dt_min": 1e-14,
                                  "end_time": 10e-9,
                                  "dt_chemistry_nmin": 1e15}
        # No Category
        self.default_simulation_vals = {"multigrid_num_vcycles": 2,
                                        "box_size": 8,
                                        "dt_safety_factor": 0.9,
                                        "time_integrator": "heuns_method",
                                        "use_end_time": True,
                                        "use_end_streamer_length": False,
                                        "end_streamer_length": 16e-3,
                                        "initial_streamer_pos_steps_wait": 5,
                                        "restart_from_file": "UNDEFINED",
                                        "memory_limit_GB": 16,
                                        "rng_seed": [8123, 91234, 12399, 293434],
                                        "species_boundary_condition": "neumann_zero"}
        # No Category
        self.default_grid_vals = {"periodic": [False] * self.n_dim,
                                  "cylindrical": False,
                                  "use_dielectric": False,
                                  "coarse_grid_size": [8] * self.n_dim,
                                  "domain_len": [16e-3] * self.n_dim,
                                  "domain_origin": [0.0] * self.n_dim}
        # Category: lineout
        self.default_lineout_vals = {"npoints": 500,
                                     "rmax": [1.0] * self.n_dim,
                                     "rmin": [0.0] * self.n_dim,
                                     "write": False}
        # Category: photoi
        self.default_photoi_vals = {"enabled": True,
                                    "eta": 0.05,
                                    "method": "helmholtz",
                                    "per_steps": 10,
                                    "quenching_pressure": 40e-3,
                                    "source_type": "Zheleznyak",
                                    "excited_species": "UNDEFINED",
                                    "photoemission_time": 0.0}

        # Category: photoi_helmh
        self.default_photoi_helmh_vals = {"author": "Luque",
                                          "coeffs": [337557.38, 19972.14],
                                          "lambdas": [4425.38, 750.06],
                                          "max_rel_residual": 1e-2}
        # Category: photoi_mc
        self.default_photoi_mc_vals = {"absorp_fac": 0.25,
                                       "const_dx": True,
                                       "min_dx": 1e-9,
                                       "num_photons": 100000,
                                       "physical_photons": True}
        # Category: plane
        self.default_plane_vals = {"npixels": [64, 64],
                                   "rmax": [1.0, 1.0],
                                   "rmin": [0.0, 0.0],
                                   "varname": "e",
                                   "write": False}
        # Category: fixes
        self.default_fixes_vals = {"diffusion_field_limit": 1e100,
                                   "drt_limit_flux": False,
                                   "max_velocity": -1.0}

        # Category: gas
        self.default_gas_vals = {"components": ["N2", "O2"],
                                 "fractions": [0.8, 0.2],
                                 "pressure": 1,
                                 "temperature": 300}
        # Category: output
        self.default_output_vals = {"dt": 1e-10,
                                    "name": "output/my_sim",
                                    "src_decay_time": -1.0,
                                    "src_term": False,
                                    "status_delay": 60,
                                    "only": []}
        # Category: datfile
        self.default_datfile_vals = {"write": False,
                                     "per_outputs": 1}

        # Category: table_data
        self.default_table_data_vals = {"max_townsend": 1e3,
                                        "min_townsend": 0.0,
                                        "size": 1000}

        # Category: input_data
        self.default_input_data_vals = {"file": "../../transport_data/td_air_siglo_swarm.txt",
                                        "old_style": True}

        # Category: field_maxima
        self.default_field_maxima_vals = {"write": False,
                                          "threshold": 0.0,
                                          "distance": 0.0}

        # Category: silo
        self.default_silo_vals = {"per_outputs": 1}

        # No Category
        self.default_circuit_vals = {"use_circuit_andy": False,
                                     "use_circuit_R": False,
                                     "use_top_electrode_sphere": False,
                                     "use_bottom_electrode_sphere": False,
                                     "top_electrode_sphere_radius": 0,
                                     "bottom_electrode_sphere_radius": 0,
                                     "capacitor_capacitance": 200e-12,
                                     "resistor": 300,
                                     "gnd_resistor": 10,
                                     "capacitor_initial_voltage_fraction": 1,
                                     "top_electrode_initial_voltage_fraction": 1,
                                     "n_streamers_circuit": 1}
        
        # No Category
        self.default_preion_channels_vals = {"t_field_off": -1}
        #################################################################


class DIPIC3DBatch(AfivoBatch):

    def __init__(self, dataset_name, produce_filename_func=default_produce_filename, produce_dirname_func=default_produce_dirname):
        print("THIS DIPIC3D BATCHER IS MADE FOR THE \"from_vtu\" BRANCH AND NOT THE MASTER BRANCH")

        AfivoBatch.__init__(self, dataset_name=dataset_name, produce_filename_func=produce_filename_func, produce_dirname_func=produce_dirname_func)

        ############### DEFAULT DICTIONARIES #########################
        # Category: domain
        self.default_domain_vals = {}
        # Category: vtu
        self.default_vtu_vals = {}
        # Category: init
        self.default_init_vals = {}
        # Category: simulation
        self.default_simulation_vals = {}
        # Category: output
        self.default_output_vals = {}
        # Category: gas
        self.default_gas_vals = {}
        # Category: grid
        self.default_grid_vals = {}
        # Category: ion
        self.default_ion_vals = {}
        # Category: voltage
        self.default_voltage_vals = {}

        self._create_default_dicts()
        ##############################################################

        ##################### USER DICTIONARIES #####################
        self.user_domain_vals = {}
        self.user_domain_batch_vars = []

        self.user_vtu_vals = {}
        self.user_vtu_batch_vars = []

        self.user_init_vals = {}
        self.user_init_batch_vars = []

        self.user_simulation_vals = {}
        self.user_simulation_batch_vars = []

        self.user_output_vals = {}
        self.user_output_batch_vars = []

        self.user_gas_vals = {}
        self.user_gas_batch_vars = []

        self.user_grid_vals = {}
        self.user_grid_batch_vars = []

        self.user_ion_vals = {}
        self.user_ion_batch_vars = []

        self.user_voltage_vals = {}
        self.user_voltage_batch_vars = []

    def set_domain_rmin(self, val_float_list):
        """
        The minimum coordinate (in m) of the domain.
        """

        self.set_list_var("rmin", val_float_list, self.user_domain_vals, self.user_domain_batch_vars, "domain")

    def set_domain_rmax(self, val_float_list):
        """
        The maximum coordinate (in m) of the domain.
        """

        self.set_list_var("rmax", val_float_list, self.user_domain_vals, self.user_domain_batch_vars, "domain")

    def set_domain_size(self, val_float_list):
        """
        The size (in cells) of the domain.
        """

        self.set_list_var("size", val_float_list, self.user_domain_vals, self.user_domain_batch_vars, "domain")

    def set_vtu_length_scale(self, val_float):
        """
        The lenth unit (in m) in the vtu file.
        """

        self.set_non_list_var("length_scale", val_float, self.user_vtu_vals, self.user_vtu_batch_vars, "vtu")

    def set_vtu_var_name(self, val_string):
        """
        Name of scalar variable in vtu file.
        """

        self.set_non_list_var("var_name", val_string, self.user_vtu_vals, self.user_vtu_batch_vars, "vtu")

    def set_vtu_file(self, val_string):
        """
        The input vtu file.
        """

        absolute_path_val = get_absolute_paths(val_string)

        self.set_non_list_var("file", absolute_path_val, self.user_vtu_vals, self.user_vtu_batch_vars, "vtu")

    def set_vtu_epsilon_file(self, val_string):
        """
        The input vtu file for epsilon.
        """

        absolute_path_val = get_absolute_paths(val_string)

        self.set_non_list_var("epsilon_file", absolute_path_val, self.user_vtu_vals, self.user_vtu_batch_vars, "vtu")

    def set_vtu_refine_voltage(self, val_float):
        """
        Refine if interpolation error is larger than this
        """

        self.set_non_list_var("refine_voltage", val_float, self.user_vtu_vals, self.user_vtu_batch_vars, "vtu")

    def set_vtu_interpolation_script(self, val_string):
        """
        Python script used for interpolationg .vtu files to a structured mesh.
        """

        absolute_path_val = get_absolute_paths(val_string)

        self.set_non_list_var("interpolation_script", absolute_path_val, self.user_vtu_vals, self.user_vtu_batch_vars, "vtu")

    def set_vtu_epsilon_gas(self, val_float):
        """
        The epsilon of the gas region as defined in the vtu files.
        """

        self.set_non_list_var("epsilon_gas", val_float, self.user_vtu_vals, self.user_vtu_batch_vars, "vtu")

    def set_init_electron_distribution(self, val_string):
        """
        Distribution of initial electrons (cylinder, sphere)
        """

        self.set_non_list_var("electron_distribution", val_string, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_electron_density(self, val_float):
        """
        The initial density of electrons uniformly distributed in the domain (m^-3).
        """

        self.set_non_list_var("electron_density", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_electron_sphere_center(self, val_float_list):
        """
        Center of the spherical distribution of electrons.
        """

        self.set_list_var("electron_sphere_center", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_electron_sphere_radius(self, val_float):
        """
        Radius of the spherical electron distribution.
        """

        self.set_non_list_var("electron_sphere_radius", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_electron_cylinder_rmin(self, val_float_list):
        """
        Min r of the cylindrical distribution of electrons.
        """

        self.set_list_var("electron_cylinder_rmin", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_electron_cylinder_rmax(self, val_float_list):
        """
        Max r of the cylindrical distribution of electrons.
        """

        self.set_list_var("electron_cylinder_rmax", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_electron_z_coord(self, val_float):
        """
        Sets the z coordinate of an on axis placed electron
        """

        self.set_non_list_var("electron_z_coord", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_distribution(self, val_string):
        """
        Distribution of initial ions (cylinder, sphere)
        """

        self.set_non_list_var("ion_distribution", val_string, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_density(self, val_float):
        """
        Density of negative ions
        """

        self.set_non_list_var("ion_density", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_sphere_center(self, val_float_list):
        """
        Center of the spherical distribution of ions.
        """

        self.set_list_var("ion_sphere_center", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_sphere_radius(self, val_float):
        """
        Radius of the spherical ion distribution.
        """

        self.set_non_list_var("ion_sphere_radius", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_cylinder_rmin(self, val_float_list):
        """
        Min r of the cylindrical distribution of ions.
        """

        self.set_list_var("ion_cylinder_rmin", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_cylinder_rmax(self, val_float_list):
        """
        Max r of the cylindrical distribution of ions.
        """

        self.set_list_var("ion_cylinder_rmax", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_z_coord(self, val_float):
        """
        Sets the z coordinate of an on axis placed ion
        """

        self.set_non_list_var("ion_z_coord", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_distribution2(self, val_string):
        """
        Distribution of initial ions (cylinder, sphere) (2nd seed)
        """

        self.set_non_list_var("ion_distribution2", val_string, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_density2(self, val_float):
        """
        Density of negative ions (2nd seed)
        """

        self.set_non_list_var("ion_density2", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_sphere_center2(self, val_float_list):
        """
        Center of the spherical distribution of ions. (2nd seed)
        """

        self.set_list_var("ion_sphere_center2", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_sphere_radius2(self, val_float):
        """
        Radius of the spherical ion distribution. (2nd seed)
        """

        self.set_non_list_var("ion_sphere_radius2", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_cylinder_rmin2(self, val_float_list):
        """
        Min r of the cylindrical distribution of ions. (2nd seed)
        """

        self.set_list_var("ion_cylinder_rmin2", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_cylinder_rmax2(self, val_float_list):
        """
        Max r of the cylindrical distribution of ions. (2nd seed)
        """

        self.set_list_var("ion_cylinder_rmax2", val_float_list, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_init_ion_z_coord2(self, val_float):
        """
        Sets the z coordinate of an on axis placed ion (2nd seed)
        """

        self.set_non_list_var("ion_z_coord2", val_float, self.user_init_vals, self.user_init_batch_vars, "init")

    def set_simulation_electron_field_threshold(self, val_float):
        """
        Remove electrons if they are in a field lower than this (V/m).
        """

        self.set_non_list_var("electron_field_threshold", val_float, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_simulation_dt(self, val_float):
        """
        The time step (s)
        """

        self.set_non_list_var("dt", val_float, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_simulation_dt_ions(self, val_float):
        """
        The time step for ions.
        """

        self.set_non_list_var("dt_ions", val_float, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_simulation_end_time(self, val_float):
        """
        The end time (s)
        """

        self.set_non_list_var("end_time", val_float, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_simulation_max_num_electrons(self, val_float):
        """
        Stop when there are this many electrons
        """

        self.set_non_list_var("max_num_electrons", val_float, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_simulation_n_runs(self, val_float):
        """
        Number of times the simulation has to completely restart. For statistics purposes.
        """

        self.set_non_list_var("n_runs", val_float, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_simulation_box_size(self, val_float):
        """
        The size (in cells) of boxes.
        """

        self.set_non_list_var("box_size", val_float, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_simulation_cs_file(self, val_string):
        """
        File containing the cross sections needed for the particle simulation.
        """

        absolute_path_val = get_absolute_paths(val_string)

        self.set_non_list_var("cs_file", absolute_path_val, self.user_simulation_vals, self.user_simulation_batch_vars, "simulation")

    def set_output_name(self, val_string_output_dir="", val_string_output_name="", use_produce_name_functions=False):
        """
        Name for the output files.  output_dir/output_filename.

        Note: This is the only function where you cannot supply more than 1 directory/filename.
        If different filenames are required only supply 1 dirname.
        If also different dirnames are required set "use_produce_name_functions" to True and provide
        the batcher with suitable functions for produce_filename_func and produce_dirname_func.

        If only output_dir is given then output_filename will be generated by the produce_filename_func

        if use_produce_name_functions is set to True then the output_directory and the output_filenames will be
        generated using the functions passed to produce_dirname_func and produce_filename_func respectively.

        """

        if type(val_string_output_dir) == list:
            print("Cannot provide more than 1 output directory")
            print("If output has to be put in different directories based on the simulation")
            print("then set \"use_produce_name_functions\" to True and provide the batcher with a suitable")
            print("produce_dirname_func")
            exit(-1)

        if type(val_string_output_name) == list:
            print("Cannot provide more than 1 filename")
            print("If output has to be named differently based on the simulation")
            print("then do not pass filenames to this function but pass a suitable \"produce_filename_func\" to the batcher")
            exit(-1)

        if not use_produce_name_functions:
            output_dir = val_string_output_dir + os.path.sep if val_string_output_dir[-1] != os.path.sep else val_string_output_dir
            val_string = output_dir + val_string_output_name
            self.set_non_list_var("name", val_string, self.user_output_vals, self.user_output_batch_vars, "output")
        else:
            self.set_non_list_var("name", "REPLACEME", self.user_output_vals, self.user_output_batch_vars, "output")
            self._use_produce_name_functions = use_produce_name_functions

    def set_output_dt_particles(self, val_float):
        """
        The output time step for particles (s).
        """

        self.set_non_list_var("dt_particles", val_float, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_log_step_interval(self, val_float):
        """
        Write to the log every N time steps.
        """

        self.set_non_list_var("log_step_interval", val_float, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_n_particles_step_interval(self, val_float):
        """
        Write the number of particles over time every N time steps
        """

        self.set_non_list_var("n_particles_step_interval", val_float, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_n_particles_over_time(self, val_bool):
        """
        Whether we want to output the amount of each particle species over time at every log step
        """

        self.set_non_list_var("n_particles_over_time", val_bool, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_particles(self, val_bool):
        """
        Whether we want to output the simulation particles at every output dt.
        """

        self.set_non_list_var("particles", val_bool, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_log(self, val_bool):
        """
        Whether we want to output the log file
        """

        self.set_non_list_var("log", val_bool, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_output_last_electron(self, val_bool):
        """
        Whether we want to output the last electron added for simulations when a discharge was initiated
        """

        self.set_non_list_var("last_electron", val_bool, self.user_output_vals, self.user_output_batch_vars, "output")

    def set_gas_pressure(self, val_float):
        """
        The gas pressure (bar).
        """

        self.set_non_list_var("pressure", val_float, self.user_gas_vals, self.user_gas_batch_vars, "gas")

    def set_gas_temperature(self, val_float):
        """
        The gas temperature (K).
        """

        self.set_non_list_var("temperature", val_float, self.user_gas_vals, self.user_gas_batch_vars, "gas")

    def set_grid_tree_dat_file(self, val_string):
        """
        The file containing the amr tree data of previously interpolated vtu files.
        """

        absolute_path_val = get_absolute_paths(val_string)

        self.set_non_list_var("tree_dat_file", absolute_path_val, self.user_grid_vals, self.user_grid_batch_vars, "grid")

    def set_ion_mass(self, val_float):
        """
        Mass of negative ion (kg)
        """

        self.set_non_list_var("mass", val_float, self.user_ion_vals, self.user_ion_batch_vars, "ion")

    def set_ion_reduced_mobility(self, val_float):
        """
        Reduced mobility of negative ion (signed) (m V s)^-1
        """

        self.set_non_list_var("reduced_mobility", val_float, self.user_ion_vals, self.user_ion_batch_vars, "ion")

    def set_ion_max_eV(self, val_float):
        """
        Max energy of negative ion (eV)
        """

        self.set_non_list_var("max_eV", val_float, self.user_ion_vals, self.user_ion_batch_vars, "ion")

    def set_voltage_amplitudes(self, val_float_list):
        """
        Relative amplitudes of the voltage
        """

        self.set_list_var("amplitudes", val_float_list, self.user_voltage_vals, self.user_voltage_batch_vars, "voltage")

    def set_voltage_times(self, val_float_list):
        """
        Times for the voltage amplitudes (s)
        """

        self.set_list_var("times", val_float_list, self.user_voltage_vals, self.user_voltage_batch_vars, "voltage")

    def write_configs(self):

        cfg_default_str = "######### DEFAULT #################\n\n"
        cfg_default_str += self._write_default_dicts_to_afivo_cfg_str()

        cfg_user_str = "######### USER #################\n\n"

        cfg_general_var_strs = self.write_var_dict_to_afivo_cfg_str(self.general_var_dict, self.general_var_batch)
        cfg_user_domain_strs = self.write_var_dict_to_afivo_cfg_str(self.user_domain_vals, self.user_domain_batch_vars)
        cfg_user_vtu_strs = self.write_var_dict_to_afivo_cfg_str(self.user_vtu_vals, self.user_vtu_batch_vars)
        cfg_user_init_strs = self.write_var_dict_to_afivo_cfg_str(self.user_init_vals, self.user_init_batch_vars)
        cfg_user_simulation_strs = self.write_var_dict_to_afivo_cfg_str(self.user_simulation_vals, self.user_simulation_batch_vars)
        cfg_user_output_strs = self.write_var_dict_to_afivo_cfg_str(self.user_output_vals, self.user_output_batch_vars)
        cfg_user_gas_strs = self.write_var_dict_to_afivo_cfg_str(self.user_gas_vals, self.user_gas_batch_vars)
        cfg_user_grid_strs = self.write_var_dict_to_afivo_cfg_str(self.user_grid_vals, self.user_grid_batch_vars)
        cfg_user_voltage_strs = self.write_var_dict_to_afivo_cfg_str(self.user_voltage_vals, self.user_voltage_batch_vars)

        cfg_user_strs = [cfg_general_var_strs, cfg_user_domain_strs, cfg_user_vtu_strs,
                         cfg_user_init_strs, cfg_user_simulation_strs, cfg_user_output_strs,
                         cfg_user_gas_strs, cfg_user_grid_strs, cfg_user_voltage_strs]

        all_combinations_of_cfg_user_strs = self._get_all_combinations_of_cfg_user_strs(cfg_user_strs)

        dataset_file = open(self.dataset_name + ".set", "w")

        for combination in all_combinations_of_cfg_user_strs:
            cfg_str = cfg_user_str

            for cfg_var in combination:
                cfg_str += cfg_var

            cfg_str += cfg_default_str

            file_name_in_output_name, output_name_val = self._is_filename_in_cfg("output%name", combination)

            # Generate custom filenames and directories for the config files
            dir_name = self.produce_dirname_func(cfg_str)
            # Create an absolute path out of the dir_name
            if dir_name[0] == "~":
                dir_name = os.path.expanduser(dir_name)
            dir_name = os.path.abspath(dir_name)

            file_name = self.produce_filename_func(cfg_str)

            if not self._use_produce_name_functions:
                # We add the generated filename from produce_filename_func to output%name
                if not file_name_in_output_name:
                    cfg_str = cfg_str.replace(output_name_val[:-1], output_name_val[:-1] + file_name)
            else:
                cfg_str = cfg_str.replace("REPLACEME", dir_name + os.path.sep + file_name)

            # If the custom directory does not exist, create it.
            if not os.path.exists(dir_name):
                os.mkdir(dir_name)

            with open(dir_name + os.path.sep + file_name + ".cfg", "w") as f:
                f.write(cfg_str)

            # Write config path to a dataset file
            dataset_file.write(dir_name + os.path.sep + file_name + ".cfg" + "\n")

        dataset_file.close()

    def _write_default_dicts_to_afivo_cfg_str(self):

        cfg_default_str = ""
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_domain_vals, self.user_domain_vals, "domain")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_vtu_vals, self.user_vtu_vals, "vtu")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_init_vals, self.user_init_vals, "init")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_simulation_vals, self.user_simulation_vals, "simulation")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_output_vals, self.user_output_vals, "output")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_gas_vals, self.user_gas_vals, "gas")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_grid_vals, self.user_grid_vals, "grid")
        cfg_default_str += self._write_default_dict_to_afivo_cfg_str(self.default_voltage_vals, self.user_voltage_vals, "voltage")

        return cfg_default_str

    def _create_default_dicts(self):

        # Category: domain
        self.default_domain_vals = {"rmin": [0.0, 0.0],
                                    "rmax": [1e-2, 1e-2],
                                    "size": [8, 8]}
        # Category: vtu
        self.default_vtu_vals = {"length_scale": 1.0,
                                 "var_name": "UNDEFINED",
                                 "file": "UNDEFINED",
                                 "epsilon_file": "UNDEFINED",
                                 "refine_voltage": 30,
                                 "interpolation_script": "scripts/interpolate_vtk.py",
                                 "epsilon_gas": 1.0}
        # Category: init
        self.default_init_vals = {"electron_distribution": "cylinder",
                                  "electron_sphere_center": [0.0, 0.0, 0.0],
                                  "electron_sphere_radius": 1e-3,
                                  "electron_cylinder_rmin": [0.0, 0.0],
                                  "electron_cylinder_rmax": [1e-2, 1e-2],
                                  "electron_density": 1e9,
                                  "electron_z_coord": 0.0,
                                  "ion_distribution": "cylinder",
                                  "ion_sphere_center": [0.0, 0.0, 0.0],
                                  "ion_z_coord": 0.0,
                                  "ion_sphere_radius": 1e-3,
                                  "ion_cylinder_rmin": [0.0, 0.0],
                                  "ion_cylinder_rmax": [1e-2, 1e-2],
                                  "ion_density": 0.0,
                                  "ion_distribution2": "cylinder",
                                  "ion_sphere_center2": [0.0, 0.0, 0.0],
                                  "ion_z_coord2": 0.0,
                                  "ion_sphere_radius2": 1e-3,
                                  "ion_cylinder_rmin2": [0.0, 0.0],
                                  "ion_cylinder_rmax2": [1e-2, 1e-2],
                                  "ion_density2": 0.0}

        # Category: simulation
        self.default_simulation_vals = {"electron_field_threshold": -1.0,
                                        "dt": 1e-13,
                                        "dt_ions": 1e-10,
                                        "end_time": 1e-9,
                                        "max_num_electrons": 1e6,
                                        "n_runs": 1,
                                        "box_size": 8,
                                        "cs_file": "input/cs_example.txt"}
        # Category: output
        self.default_output_vals = {"name": "output/sim",
                                    "dt_particles": 1e-10,
                                    "log_step_interval": 100,
                                    "n_particles_step_interval": 100,
                                    "n_particles_over_time": False,
                                    "particles": True,
                                    "log": True,
                                    "last_electron": False}
        # Category: gas
        self.default_gas_vals = {"pressure": 1,
                                 "temperature": 300}
        # Category: grid
        self.default_grid_vals = {"tree_dat_file": "UNDEFINED"}

        # Category: ion
        C_atomic_mass = 1.66053904e-27  # kg
        self.default_ion_vals = {"mass": 32 * C_atomic_mass,
                                 "reduced_mobility": -7.1e21,
                                 "max_eV": 20.0}

        # Category: voltage
        self.default_voltage_vals = {"amplitudes": [1.0, 1.0],
                                     "times": [0.0, 1.0]}
