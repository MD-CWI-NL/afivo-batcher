#!/usr/bin/env python3

# A script to generate a list of commands for parameter studies

import itertools

# Start of user code
base_command = './streamer streamer_2d.cfg'
parameters = []

parameters.append(
    {'name': 'field_amplitude',
     'format': '_fld={:.2g}',
     'values': [2.0e6, 2.5e6]})

parameters.append(
    {'name': 'background_density',
     'format': '_n0={:.2g}',
     'values': [1e13, 1e14, 1e15]})

# These parameters will be varied together
groups = []

groups.append(
    {'name': ['seed_density', 'seed_falloff'],
     'format': '_seed',
     'values': [[1e13, 1e14, 1e15],
                ['smoothstep', 'gaussian', 'gaussian']]})
# End of user code

short_name_prefix = '-output%name+='


# To print arrays without []
def value_to_string(v):
    if hasattr(v, "__len__") and (not isinstance(v, str)):
        return ' '.join([str(x) for x in v])
    else:
        return v


# Generate short names for the parameters
for par in parameters:
    short = [par['format'].format(v) for v in par['values']]

    # Make sure names are unique (add number otherwise)
    if len(short) != len(set(short)):
        short = [s + str(i) for i, s in enumerate(short)]

    par['short_names'] = short

# Generate short names for the parameter groups
for grp in groups:
    n_values = len(grp['values'][0])
    grp['short_names'] = [grp['format'] + str(i) for i in range(n_values)]

# Generate commands
for par in parameters:
    cmds = []

    for v, s in zip(par['values'], par['short_names']):
        cmds.append(r"-{}='{}' {}{}".format(
            par['name'], value_to_string(v), short_name_prefix, s))
    par['commands'] = cmds

# Generate commands for groups
for grp in groups:
    cmds = []

    # Vary the parameters together
    for i in range(len(grp['short_names'])):
        cmd = "{}{}".format(short_name_prefix, grp['short_names'][i])

        for name, vals in zip(grp['name'], grp['values']):
            v = vals[i]
            cmd += r" -{}='{}'".format(name, value_to_string(v))
        cmds.append(cmd)
    grp['commands'] = cmds

option_list = [par['commands'] for par in parameters + groups]
option_permutations = itertools.product(*option_list)

for opt in option_permutations:
    print(base_command + ' ' + ' '.join(opt))
