from afivobatcher.batcher import AfivoStreamerBatch
from afivobatcher.configreader.afivostreamer import *
import sys
import os
"""
Afivo-Streamer batch example written by Andy Martinez 14/08/2019.

In this example we will create config files to perform a set of simulations
where the initial seed density is varied.


NOTE: to create config files that you can run on your machine
      change the variable "afivo_streamer_directory" to the directory
      that holds afivo-streamer on your machine.


After running this script 3 config files (.cfg) and a dataset file (.set) will be produced.
The dataset file stores the paths to the 3 config files.

You can now run the simulations individually as you would do normally using
the config files, but you can also use the dataset file to loop through all
the config files created and run all the simulations of the dataset with 1 command.

To run all the config files with 1 command we can use the shell script "run_dataset.sh"

From the afivo-batcher folder we can run:

./run_dataset.sh EXECUTABLE DATASET NUM_THREADS

where EXECUTABLE is the simulation program that you want to use with the config files
(in this case afivo-streamer/programs/standard_2d/streamer)

and where DATASET is the dataset file created alongside the config files
(in this case initial_seed_variation.set)

Whenever you are in doubt about what paths should be what, use absolute paths.
Absolute paths will always work.

A relative path is "~/Documents"
An absolute path is "/home/andy/Documents" (starts from "/")
"""


# We create our own version of the produce_filename function.
# This will allow us to create filenames for our config files
# and output files in the format that is specific to the current application.
# This produce_filename function takes the content of the .cfg file as input
# and uses functions in the afivobatcher.filenamer package to extract variable values from it.
# The default filenames are "default_filename_(index)". So for testing purposes
# produce_filename and produce_dirname do not need to be created.
def produce_filename(cfg_string):
    # All the config variables are stored in the cfg_string as text.
    # If we want to base our filenames on what value a certain variable has
    # then we can extract it from the cfg_string using a couple of supplied functions

    # We want to use our dataset_name as a prefix to the filenames
    filename = dataset_name + "_"

    # We want to base our filename on the density of the seed that we added.
    # To extract the seeds from the cfg string we can do the following:
    seeds = get_seeds_from_cfg_string(cfg_string)

    # The density that we are interested in is that of the seed that we added first
    seed_density = get_seed_density_from_seed(seeds[0])

    # We want a seed density of 5 * 10^16 to be displayed as 5e16
    seed_density_string = "%.0e" % seed_density  # gives us 5e+16
    seed_density_string = seed_density_string.replace("+", "")  # removes the +, giving us 5e16

    filename += "N" + seed_density_string

    # We can also add an extra tag for the electric field strength
    electric_field = get_field_amplitude_from_cfg_string(cfg_string)
    electric_field_string = "%.0e" % electric_field
    electric_field_string = electric_field_string.replace("+", "")

    filename += "E" + electric_field_string

    return filename


# If we want to put the config files in a custom directory (which will be created if it does not exist),
# then we also need to create a function that produces a name for the directory based on the content of the .cfg files.
def produce_dirname(cfg_str):
    # In this case we will create the configs in the current directory.
    # This is also the default behavior so there is no need to create
    # this function if this is the wanted behavior
    #return "."

    # A commonly used dirname is to use the same name as the filenames.
    # This will organize all simulation output nicely according to the logic in produce_filename
    return "./" + produce_filename(cfg_str)


if len(sys.argv) < 2:
    print("Missing afivo-streamer directory !")
    print("Correct usage: python3 example.py directory_to_afivo-streamer")
    sys.exit()

dataset_name = "initial_seed_variation"

# Change this to your afivo-streamer dir
afivo_streamer_directory = sys.argv[1]
# We create an absolute path in case the user gave a relative path.
if afivo_streamer_directory[0] == "~":
    afivo_streamer_directory = os.path.expanduser(afivo_streamer_directory)
afivo_streamer_directory = os.path.abspath(afivo_streamer_directory)


# Create an AfivoStreamerBatch instance
AB = AfivoStreamerBatch(dataset_name=dataset_name, n_dim=2, produce_filename_func=produce_filename, produce_dirname_func=produce_dirname)

# Set the variables that we want to have in the streamer simulation

# Set the background electric field to 3 MV / m
AB.set_field_amplitude(3e6)

# Set the domain size to be 30 mm x 30 mm
AB.set_domain_len([30e-3, 30e-3])

# End the simulation after 9 ns
AB.set_end_time(9e-9)

# We want a cylindrical simulation
AB.set_cylindrical(True)

# Enable helmholtz photoionization using bourdon coeffs
AB.set_photoi_enabled(True)
AB.set_photoi_method("helmholtz")
AB.set_photoi_per_steps(1)
AB.set_photoi_helm_author("Bourdon")

# We now want to add a seed but we want its density to be varied in different simulations
seed_charge_type = 0  # Neutral seed
seed_rel_r0 = [0.0, 0.0]  # Start relative coordinates of seed
seed_rel_r1 = [0.0, 0.05] # End relative coordinates of seed
seed_width = 2e-4
seed_fallof = "gaussian"

# Normally the seed density is only 1 value per seed
# but because we want to run multiple simulations
# where the seed density is different we now make a list of seed_density values.
# We want the seed density to vary from 5e17 to 5e19 across 3 simulations
seed_density = [5 * 10**x for x in range(17, 20)]

# We see that every parameter in every "set" function has its type in the name ("float", "list", "string", "bool")
# this is to make it easier for people to know what kind of value you need to pass.
# To make a variable be a batch variable (like our seed_density) simply provide a list of values
# of the mentioned variable type. This works for EVERY variable
# Example: to batch a "float" var you pass a list of floats
# Example: to batch a "list" var (like coordinates) you pass a list of lists
AB.add_seed(seed_charge_type_float=seed_charge_type,
            seed_rel_r0_list=seed_rel_r0,
            seed_rel_r1_list=seed_rel_r1,
            seed_width_float=seed_width,
            seed_falloff_string=seed_fallof,
            seed_density_float=seed_density)

# Here we could add more seeds with each their own batch variable


# Whenever paths to files or directories have to be supplied,
# remember that these are relative to the config file path.
# If you want to be completely safe, you can use absolute paths.
AB.set_input_data_file(os.path.join(afivo_streamer_directory, "transport_data/td_air_siglo_swarm.txt"))
AB.set_input_data_old_style(True)

# The output name can be split in 2 parts: The output directory, and the output filenames
# If only the output_directory is set then the output filename will be produced by myFileNamer.produce_filename
#AB.set_output_name(os.path.join(afivo_streamer_directory, "programs/standard_2d/output"))

# We can also set the output to point to the same directories and filenames produced by our produce_dirname functions
AB.set_output_name(use_produce_name_functions=True)


# As a final step we need to write the config files
AB.write_configs()

