from afivobatcher.runner.remote import RemoteRunner
import sys


"""
Afivo-Streamer batch example written by Andy Martinez 14/08/2019.

BEFORE RUNNING THIS SCRIPT FIRST RUN "seed_density_batch_SSH.py"


This example script shows how we can submit a dataset to a remote computer like perun.md.cwi.nl which does NOT have
a job scheduler like PBS or SLURM


In this example a job script will be created for every config file
in the passed dataset. Next to the job scripts 2 more files will be
created: a dataset_jobs.set and a dataset_jobs_remote.set file.
The dataset_jobs.set file stores all absolute paths to the job scripts
created for every config file.
The dataset_jobs_remote.set stores all absolute paths to where the job
scripts will be copied to on the remote host. We need these paths to submit
the jobs.

When you run this script there will be prompts asking you for:
- remote host without job scheduler  (like perun.md.cwi.nl)
- remote_username (your username on that host: remote_username@remote_host is what you use to ssh into it)
- path to the executable on the remote host (The job scripts need to know what program to call)
- path to script destination on the remote host (We need to copy both the job scripts and config files to the remote. This is the directory we copy them to)


2 jobs will be run on the remote host: One will have 2 simulations running in parallel, the other one has the final simulation running.
All jobs (not the simulations) run serially on the remote host.

Whenever you are in doubt about what paths should be what, use absolute paths.
Absolute paths will always work.

A relative path is "~/Documents"
An absolute path is "/home/andy/Documents" (starts from "/")
"""


if len(sys.argv) < 2:
    print("Missing path to initial_seed_variations.set !")
    print("Correct usage: python3 run_seed_density_batch_remote.py path/to/dataset.set")
    sys.exit()

# In most use cases, these are the only variables you need to define
local_path_to_dataset = sys.argv[1]
remote_host = input("Enter name of remote host (e.g. perun.md.cwi.nl): ")
remote_username = input("Enter username on remote host: ")
remote_path_to_executable = input("Enter remote path to the executable that will be used to run the configs: ")
remote_path_to_script_destination = input("Enter remote path where config files can be copied to: ")
remote_host_scratch_dir = input("Enter path to fast output directory (same as entered when running \"seed_density_batch_SSH.py\"")

# Initialize the runner
# Note that there is a variable called "copy_scripts_with_custom_dir":
# If we made a custom directory for every config file and we would like
# to keep this directory structure on the remote we need to set this variable to True
# it will then copy the entire custom directory instead of only the job and .cfg scripts within
runner = RemoteRunner(remote_username=remote_username,
                         remote_host=remote_host,
                         remote_path_to_executable=remote_path_to_executable,
                         local_path_to_dataset=local_path_to_dataset,
                         remote_path_to_script_destination=remote_path_to_script_destination,
                         copy_scripts_with_custom_dir=True)

# Everything between the initialization of the Runner and the writing of the job scripts
# can be used to customize how the job scripts are build.
# This can range from making every resource requirement of the computing node
# to be dependent on the variables in the config files (e.g. higher electric fields require a longer walltime)
# to adding extra custom commands to the job script before, during, or after the execution of the main executable

# The default resources asked are:
# walltime = Until process exits on its own
runner.set_walltime(minutes=5)
# cores = 8


# We can also exit the simulation a given amount of time before the walltime ends.
# This gives us the opportunity to copy files from a temporary fast directory (like /scratch/) to our own
# directories so that we do not lose data
runner.set_timeout_before_walltime(minutes=2)  # This will end the simulation after walltime - timeout = 3 minuts


# We can run multiple simulations per job and split the number of cores between them
runner.set_n_simulations_per_job(2)

# if we want to pass our own function which creates a walltime
# based on the values in the config file we can pass a function like so:
# runner.set_walltime_from_cfg_str_func( my_custom_func )
# All custom functions take a cfg_str as input
# and usually output 1 number or string. Check the custom function
# examples in afivobatcher.runner.remote.py


# This function will add "mv" command after the execution of the main executable.
# The added command will be: mv remote_output_dir/output_filename* job_script_dir/
# In other words, it will move all output of the job from remote_output_dir to the directory that the job script is in
# Usually this is used when we are outputting to a /scratch/ directory or some other temporary but fast directory
runner.move_output_on_remote_to_job_script_dir(remote_output_dir=remote_host_scratch_dir)


# Write job scripts locally
# This will create all job scripts and job.set files used for submitting the job
runner.write_job_scripts()

# Submit job scripts on remote
# This function will first copy all job and config scripts to the
# designated remote directory. This uses an scp command and will ask for your remote password.
# After copying it will ssh into the remote and submit all job scripts.
# This action will again ask for your remote password.
runner.submit_jobs()

