# afivo-batcher

The scripts in this project will allow the creation of 100s of config files for afivo derived simulation program. In general these scripts will work for any program that uses https://github.com/jannisteunissen/config_fortran for their config parser.

The scructure of the classes in the scripts are as follows: `AfivoBatch` is the base class that contains all necessary functions to create config files. For any specific simulation program it could be useful to derive from this class to add program specific functions.
This has been done for https://gitlab.com/MD-CWI-NL/afivo-streamer where the derived batch class is called `AfivoStreamerBatch`. This derived class contains functions to manipulate all `afivo-streamer` variables in an easy way.
This has also been done for https://gitlab.com/MD-CWI-NL/DIPIC3D/tree/fromvtu where the derive batch class is called `DIPIC3DBatch`.

To create a specific structure and naming scheme for the config files you can pass a produce_filename(cfg_content) or produce_dirname(cfg_content) function to any of the AfivoBatch classes.
Functions to easily extract variables values from the config file content can be found in the afivobatch.configreader subpackage. It is not mandatory to make these functions, but the default behavior will not be 
sufficient for real world purposes only for testing.

In short: 

If you want a batcher for `afivo-streamer`: USE `AfivoStreamerBatch`

If you want a batcher for `DIPIC3D`: USE `DIPIC3DBatch`

If you want a batcher for a simulation program using https://github.com/jannisteunissen/config_fortran as a parser which doesn't have a dedicated batcher class yet: USE `AfivoBatch` (or create your own batcher by deriving from AfivoBatch) 


# Installation

To install this package clone the repository and go to the directory:

    git clone https://gitlab.com/MD-CWI-NL/afivo-batcher
    cd afivo-batcher

We can now run the scripts from this folder or install the package so that the classes and functions can be used in any python script you develop.

To install:

    python3 setup.py install

Note that you can use the name of your virtual environment instead of `python3` which is the global python v3 environment. Python v2 is not supported.

# Updating

To get and use the latest version of afivo-batcher in your python projects we first have to pull changes from the master branch of this repository and then run setup.py again to reflect those changes in the installed python package
In the afivo-batcher directory do:

    git pull
    python3 setup.py install


# Examples - LOCAL

In the examples folder you will find a heavily commented example of a batch script for afivo-streamer.
To run this example:

    cd examples/afivo-streamer/local
    python3 seed_density_batch_local.py path/to/local-afivo-streamer-directory

This will create 3 .cfg files and 1 .set file in the examples/afivo-streamer-local folder. You can run each .cfg as usual with the standard 2D cylindrical afivo-streamer executable or you can run all of the simulations with 1 command using the `run_dataset.sh` script:
From the examples/afivo-streamer/local folder:

    ./../../../run_dataset.sh path/to/afivo-streamer-executable path/to/example.set
Or from the base afivo-batcher directory:

    ./run_dataset.sh path/to/afivo-streamer-executable path/to/example.set
    

# Examples - REMOTE

There are also 2 examples that show how to make a set of config files for afivo-streamer and either submit them to a remote machine running a job scheduler (SLURM or PBS) like lisa.surfsara.nl OR 
run the set of simulations on a remote machine without job scheduler (like our own personal computers accessible by SSH)

To run the remote job scheduler example:

    cd examples/afivo-streamer/remote
    python3 seed_density_batch_LISA.py path/to/remote/afivo-streamer-directory

This will create 3 directories with each directory containing a .cfg file. This will also create 1 .set file in the examples/afivo-streamer/remote folder.
To write job scripts for each config file and submit each config to a remote host with a job scheduler  we can do:

    python3 run_seed_density_batch_LISA.py path/to/initial_seed_variation.set

If we want to run the remote NON-job-scheduler examples we can just replace `LISA` in the previous examples with `SSH`

These script will prompt the user for a couple of remote host variables. After this it will create job scripts for every config file.
Then it will copy all configs and job scripts to the remote host (this will prompt the user for a password) and after this it will submit every job script (this will again prompt the user for a password)


